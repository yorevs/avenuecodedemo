/*****************************************************************************
 * Project: <B>AvenueTestDemo</B>
 * File: <I>ValidatorEventRegister.java</I>
 * Author: <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * Creation: Jan 17, 2018
 * --------------------------------------------------------------------------
 * <I>Copyright 2018, <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * All rights are reserved. Reproduction in whole or part is prohibited
 * without the written consent of the copyright owner.</I>
 *****************************************************************************/

package br.edu.avenuecode.demo.config;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.event.ValidatingRepositoryEventListener;
import org.springframework.validation.Validator;

/**
 * Configuration class: Validators and related features.
 * TODO: This class config is to use later to implement validate methods if time
 * goes well
 *
* @author <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * @created Jan 17, 2018
 * @since JDK 1.8
 */
@Configuration
public class ValidatorEventRegister implements InitializingBean {
    
    @Autowired
    ValidatingRepositoryEventListener validatingRepositoryEventListener;
    
    @Autowired
    private Map<String, Validator> validators;
    
    @Override
    public void afterPropertiesSet() throws Exception {
        
        List<String> events = Arrays.asList( "beforeCreate", "afterCreate", "beforeSave", "afterSave", "beforeLinkSave", "afterLinkSave", "beforeDelete", "afterDelete" );
        
        for ( Map.Entry<String, Validator> entry : validators.entrySet() ) {
            events.stream().filter( p -> entry.getKey().startsWith( p ) ).findFirst().ifPresent( p -> validatingRepositoryEventListener.addValidator( p, entry.getValue() ) );
        }
    }
}
