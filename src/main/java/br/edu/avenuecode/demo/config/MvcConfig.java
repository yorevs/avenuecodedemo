/*****************************************************************************
 * Project: <B>AvenueTestDemo</B>
 * File: <I>MvcConfig.java</I>
 * Author: <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * Creation: Jan 17, 2018
 * --------------------------------------------------------------------------
 * <I>Copyright 2018, <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * All rights are reserved. Reproduction in whole or part is prohibited
 * without the written consent of the copyright owner.</I>
 *****************************************************************************/

package br.edu.avenuecode.demo.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

/**
 * Configuration class: MVC and related features.
 *
* @author <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * @created Jan 17, 2018
 * @since JDK 1.8
 */
@Configuration
@EnableWebMvc
public class MvcConfig extends WebMvcConfigurerAdapter {

    public MvcConfig() {

        super();
    }

    @Override
    public void configureDefaultServletHandling( final DefaultServletHandlerConfigurer configurer ) {

        configurer.enable();
    }
    
    /**
     * Internal resource view resolver.
     *
     /* @return TODO documentation comment here the internal resource view resolver
     */
    @Bean
    public InternalResourceViewResolver internalResourceViewResolver() {

        InternalResourceViewResolver viewResolver = new InternalResourceViewResolver();
        viewResolver.setViewClass( JstlView.class );
        viewResolver.setPrefix( "/WEB-INF/view/" );
        viewResolver.setSuffix( ".html" );
        
        return viewResolver;
    }
}
