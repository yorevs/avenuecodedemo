/*****************************************************************************
 * Project:   <B>demo</B>
 * File:      <I>ProductService.java</I>
 * Author:    <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * Creation:  Jan 15, 2018
 * --------------------------------------------------------------------------
 * <I>Copyright 2018, <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * All rights are reserved. Reproduction in whole or part is prohibited
 * without the written consent of the copyright owner.</I>
 *****************************************************************************/
package br.edu.avenuecode.demo.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.avenuecode.demo.models.Product;
import br.edu.avenuecode.demo.repositories.ProductRepository;

/**
 * Class responsible to access and manipulate the Product(entity) object's repository
 *
* @author <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * @created Jan 15, 2018
 * @since JDK 1.8
 */
@Service
public class ProductService {
    
    @Autowired
    private ProductRepository productRepository;
    
    public List<Product> readAllProducts() {
        
        ArrayList<Product> allProducts = new ArrayList<Product>();
        productRepository.findAll().forEach( allProducts::add );
        
        return allProducts;
    }

    public Product readProduct( final Long productId ) {

        return productRepository.findOne( productId );
    }
    
    public List<Product> readParentProducts( final Long productId ) {
        
        return productRepository.findByParentId( productId );
    }
    
    public void createProduct( final Product product ) {
        
        productRepository.save( product );
    }

    public void updateProduct( final Product product ) {

        productRepository.save( product );
    }

    public void deleteProduct( final Long productId ) {
        
        productRepository.delete( productId );
    }

}
