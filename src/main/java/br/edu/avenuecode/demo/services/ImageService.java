/*****************************************************************************
 * Project: <B>demo</B>
 * File: <I>ImageService.java</I>
 * Author: <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * Creation: Jan 15, 2018
 * --------------------------------------------------------------------------
 * <I>Copyright 2018, <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * All rights are reserved. Reproduction in whole or part is prohibited
 * without the written consent of the copyright owner.</I>
 *****************************************************************************/

package br.edu.avenuecode.demo.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.edu.avenuecode.demo.models.Image;
import br.edu.avenuecode.demo.repositories.ImageRepository;

/**
 * Class responsible to access and manipulate the Image(entity) object's repository
 *
 * @author <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * @created Jan 15, 2018
 * @since JDK 1.8
 */
@Service
public class ImageService {

    @Autowired
    private ImageRepository imageRepository;
    
    public List<Image> readAllImages() {

        ArrayList<Image> allImages = new ArrayList<>();
        imageRepository.findAll().forEach( allImages::add );

        return allImages;
    }

    public Image readImage( final Long id ) {

        return imageRepository.findOne( id );
    }

    public List<Image> readAllProductImages( final Long productId ) {

        ArrayList<Image> allImages = new ArrayList<>();
        imageRepository.findByProductId( productId ).forEach( allImages::add );

        return allImages;
    }

    public Image readProductImageById( final Long productId, final Long imageId ) {

        return imageRepository.findByProductId( productId ).stream().filter( i -> i.getId() == imageId ).findFirst().orElse( null );
    }

    public void createImage( final Image image ) {
        
        imageRepository.save( image );
    }
    
    public void updateImage( final Image image ) {
        
        imageRepository.save( image );
    }
    
    public void deleteImage( final Long imageId ) {

        imageRepository.delete( imageId );
    }
}
