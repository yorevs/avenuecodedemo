/*****************************************************************************
 * Project:   <B>demo</B>
 * File:      <I>DemoApplication.java</I>
 * Author:    <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * Creation:  Jan 15, 2018
 * --------------------------------------------------------------------------
 * <I>Copyright 2018, <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * All rights are reserved. Reproduction in whole or part is prohibited
 * without the written consent of the copyright owner.</I>
 *****************************************************************************/
package br.edu.avenuecode.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Spring Boot Application entry point (main)
 *
 * @author hugo
 * @created Jan 18, 2018
 * @since JDK 1.8
 */
@SpringBootApplication
public class DemoApplication {
    
    public static void main( String[] args ) {
        
        SpringApplication.run( DemoApplication.class, args );
    }
}
