/*****************************************************************************
 * Project: <B>demo</B>
 * File: <I>ProductRepository.java</I>
 * Author: <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * Creation: Jan 15, 2018
 * --------------------------------------------------------------------------
 * <I>Copyright 2018, <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * All rights are reserved. Reproduction in whole or part is prohibited
 * without the written consent of the copyright owner.</I>
 *****************************************************************************/

package br.edu.avenuecode.demo.repositories;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.edu.avenuecode.demo.models.Product;

/**
 * Database CRUD repository for the entity Product
 * Naming pattern: findBy<Field1CamelCaseField2CamelCase....>
 *
* @author <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * @created Jan 15, 2018
 * @since JDK 1.8
 */
public interface ProductRepository extends CrudRepository<Product, Long> {

    /**
     * Special method to find a Product's parent Product
     *
     * @param productId
     * @return
     * @since JDK 1.8
     */
    public List<Product> findByParentId( final Long productId );

}
