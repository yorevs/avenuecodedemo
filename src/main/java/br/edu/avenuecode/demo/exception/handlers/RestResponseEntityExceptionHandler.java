/*****************************************************************************
 * Project: <B>AvenueTestDemo</B>
 * File: <I>Thist.java</I>
 * Author: <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * Creation: Jan 17, 2018
 * --------------------------------------------------------------------------
 * <I>Copyright 2018, <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * All rights are reserved. Reproduction in whole or part is prohibited
 * without the written consent of the copyright owner.</I>
 *****************************************************************************/

package br.edu.avenuecode.demo.exception.handlers;

import java.util.stream.Collectors;

import org.springframework.data.rest.core.RepositoryConstraintViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Exception handlers for REST responses
 *
* @author <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * @created Jan 17, 2018
 * @since JDK 1.8
 */
@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
    
    @ExceptionHandler( { RepositoryConstraintViolationException.class } )
    public ResponseEntity<Object> handleAccessDeniedException( final Exception ex, final WebRequest request ) {
        
        RepositoryConstraintViolationException nevEx = (RepositoryConstraintViolationException) ex;
        
        String errors = nevEx.getErrors().getAllErrors().stream().map( ObjectError::toString ).collect( Collectors.joining( "\n" ) );

        return new ResponseEntity<>( errors, new HttpHeaders(), HttpStatus.NOT_ACCEPTABLE );
    }
    
}
