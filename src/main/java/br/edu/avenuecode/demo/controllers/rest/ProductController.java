/*****************************************************************************
 * Project: <B>demo</B>
 * File: <I>ProductController.java</I>
 * Author: <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * Creation: Jan 15, 2018
 * --------------------------------------------------------------------------
 * <I>Copyright 2018, <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * All rights are reserved. Reproduction in whole or part is prohibited
 * without the written consent of the copyright owner.</I>
 *****************************************************************************/

package br.edu.avenuecode.demo.controllers.rest;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.edu.avenuecode.demo.models.Product;
import br.edu.avenuecode.demo.models.dto.ProductDTO;
import br.edu.avenuecode.demo.services.ProductService;

/**
 * Class responsible to customize the REST services related to Products(entity)
 *
* @author <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * @created Jan 15, 2018
 * @since JDK 1.8
 */
@RestController
public class ProductController {

    @Autowired
    private ProductService _productService;

    @Autowired
    private ModelMapper _modelMapper;

    private ProductDTO convertToDto( final Product product ) {

        ProductDTO productDto = _modelMapper.map( product, ProductDTO.class );

        return productDto;
    }

    // ------------------------------------------------------------------------
    // READ operations
    // ------------------------------------------------------------------------

    /**
     * A. Get all products regardless of any parent product
     * - URL: /products
     * - Method: GET
     * - Headers: [Content-Type] = application/json
     * - Body: empty
     *
     * @return JSON text representing an array of all system products including
     *         specified relationships (child product and/or images)
     * @since JDK 1.8
     */
    @RequestMapping( method = RequestMethod.GET, value = "/products"/* , headers = { "content-type=application/json" } */ )
    @ResponseStatus( HttpStatus.OK )
    public List<Product> getAllProducts() {
        
        return _productService.readAllProducts();
    }

    /**
     * B. Get all products regardless of any parent product (EXCLUDING
     * RELATIONS)
     * - URL: /products/dto
     * - Method: GET
     * - Headers: [Content-Type] = application/json ;
     * - Body: empty
     *
     * @return JSON text representing an array of all system products excluding
     *         relationships (child products, images)
     * @since JDK 1.8
     */
    @RequestMapping( method = RequestMethod.GET, value = "/products/dto"/* , headers = { "content-type=application/json" } */ )
    @ResponseStatus( HttpStatus.OK )
    public List<ProductDTO> getAllDtoProducts() {
        
        List<Product> products = _productService.readAllProducts();

        return products.stream().map( prod -> convertToDto( prod ) ).collect( Collectors.toList() );
    }

    /**
     * C. Get one product based on a specified ID
     * - URL: /products/{productId}
     * - Method: GET
     * - Headers: [Content-Type] = application/json
     * - Body: empty
     *
     * @param productId
     * @return JSON text representing the specified product including specified
     *         relationships (child product and/or images)
     * @since JDK 1.8
     */
    @RequestMapping( method = RequestMethod.GET, value = "/products/{productId}"/* , headers = { "content-type=application/json" } */ )
    @ResponseStatus( HttpStatus.OK )
    public Product getProductById( final @PathVariable
    Long productId ) {
        
        return _productService.readProduct( productId );
    }

    /**
     * D. Get one product based on a specified ID (EXCLUDING RELATIONS)
     * - URL: /products/dto/{productId}
     * - Method: GET
     * - Headers: [Content-Type] = application/json
     * - Body: empty
     *
     * @return JSON text representing the specified product excluding
     *         relationships (child products, images)
     * @since JDK 1.8
     */
    @RequestMapping( method = RequestMethod.GET, value = "/products/dto/{productId}"/* , headers = { "content-type=application/json" } */ )
    @ResponseStatus( HttpStatus.OK )
    public ProductDTO getDtoProductById( final @PathVariable
    Long productId ) {
        
        Product product = _productService.readProduct( productId );

        return convertToDto( product );
    }

    /**
     * ###### E. Get all products of a specified parent product
     * - URL: /products/{productId}/products
     * - Method: GET
     * - Headers: [Content-Type] = application/json
     * - Body: empty
     * - Return: JSON text representing an array of all product related products
     * including specified relationships (child product and/or images)
     *
     * @param productId
     * @return
     * @since JDK 1.8
     */
    @RequestMapping( method = RequestMethod.GET, value = "/products/{productId}/products"/* , headers = { "content-type=application/json" } */ )
    @ResponseStatus( HttpStatus.OK )
    public List<Product> getParentProducts( final @PathVariable
    Long productId ) {

        return _productService.readParentProducts( productId );
    }

    /**
     * F. Get all products of a specified parent product (EXCLUDING RELATIONS)
     * - URL: /products/dto/{productId}/products
     * - Method: GET
     * - Headers: [Content-Type] = application/json
     * - Body: empty
     * - Return: JSON text representing an array of all product related products
     * excluding relationships (child products, images)
     *
     * @param productId
     * @return
     * @since JDK 1.8
     */
    @RequestMapping( method = RequestMethod.GET, value = "/products/dto/{productId}/products"/* , headers = { "content-type=application/json" } */ )
    @ResponseStatus( HttpStatus.OK )
    public List<ProductDTO> getDtoParentProducts( final @PathVariable
    Long productId ) {

        List<Product> products = _productService.readParentProducts( productId );

        return products.stream().map( prod -> convertToDto( prod ) ).collect( Collectors.toList() );
    }

    // ------------------------------------------------------------------------
    // CREATE operations
    // ------------------------------------------------------------------------

    /**
     * A. Create a new product regardless of any parent product
     * - URL: /products
     * - Method: POST
     * - Headers: [Content-Type] = application/json
     * - Body:
     * {
     * "name": "<product_name>",
     * "description": "<product_description>",
     * "images": [ <image1_json>, <image2_json>, ... ],
     * "products": [ <child1_prod_json>, <child2_prod_json>, ... ],
     * "parent": <parent_project>
     * }
     * 
     * @param product
     * @since JDK 1.8
     */
    @RequestMapping( method = RequestMethod.POST, value = "/products"/* , headers = { "content-type=application/json" } */ )
    @ResponseStatus( HttpStatus.CREATED )
    public void createProduct( final @RequestBody
    Product product ) {
        
        _productService.createProduct( product );
    }

    /**
     * B. Create a new product for a specific parent product
     * - URL: /products/{productId}/products
     * - Method: POST
     * - Headers: [Content-Type] = application/json
     * - Body:
     * {
     * "name": "<product_name>",
     * "description": "<product_description>",
     * "images": [ <image1_json>, <image2_json>, ... ],
     * "products": [ <child1_prod_json>, <child2_prod_json>, ... ]
     * }
     *
     * @param product
     * @since JDK 1.8
     */
    @RequestMapping( method = RequestMethod.POST, value = "/products/{productId}/products"/* , headers = { "content-type=application/json" } */ )
    @ResponseStatus( HttpStatus.CREATED )
    public void createParentProduct( final @RequestBody
    Product product, final @PathVariable
    Long productId ) {

        Product parentProduct = _productService.readProduct( productId );
        product.setParent( parentProduct );
        _productService.createProduct( product );
    }

    // ------------------------------------------------------------------------
    // UPDATE operations
    // ------------------------------------------------------------------------

    /**
     * A. Update an existing product
     * - URL: /products/{productId}
     * - Method: PUT
     * - Headers: [Content-Type] = application/json
     * - Body:
     * {
     * "name": "<upd_product_name>",
     * "description": "<upd_product_description>",
     * "images": [ <upd_image1_json>, <upd_image2_json>, ... ],
     * "products": [ <upd_child1_prod_json>, <upd_child2_prod_json>, ... ]
     * }
     *
     * @param product
     * @param productId
     * @since JDK 1.8
     */
    @RequestMapping( method = RequestMethod.PUT, value = "/products/{productId}"/* , headers = { "content-type=application/json" } */ )
    @ResponseStatus( HttpStatus.OK )
    public void updateProduct( final @RequestBody
    Product product, final @PathVariable
    Long productId ) {

        product.setId( productId );
        _productService.updateProduct( product );
    }

    /**
     * B. Associate an existing product to a parent product
     * - URL: /products/{productId}/products
     * - Method: PUT
     * - Headers: [Content-Type] = text/uri-list
     * - Body:
     * <product_url>
     * <product_url>
     *
     * @param productUrls
     * @param productId
     * @since JDK 1.8
     */
    @RequestMapping( method = RequestMethod.PUT, value = "/products/{productId}/products", headers = { "content-type=text/uri-list" } )
    @ResponseStatus( HttpStatus.OK )
    public void updateParentProduct( final @RequestBody
    String productUrls, final @PathVariable
    Long productId ) {

        List<String> urlList = Arrays.asList( productUrls.split( "\n" ) );
        Product product = _productService.readProduct( productId );

        urlList.forEach( u -> {
            
            String idRef = u.substring( u.lastIndexOf( '/' ) + 1 );
            
            if ( idRef != null && idRef.matches( "[0-9]*" ) ) {
                
                Long childProductId = Long.parseLong( idRef );
                Product refProject = _productService.readProduct( childProductId );
                
                if ( refProject != null ) {
                    
                    refProject.setParent( product );
                    _productService.updateProduct( refProject );
                }
            }
        } );
    }

    // ------------------------------------------------------------------------
    // DELETE operations
    // ------------------------------------------------------------------------

    /**
     * A. Delete an existing product
     * - URL: /products/{productId}
     * - Method: DELETE
     * - Headers: empty
     * - Body: empty
     * - Return: empty
     *
     * @param productId
     * @since JDK 1.8
     */
    @RequestMapping( method = RequestMethod.DELETE, value = "/products/{productId}" )
    @ResponseStatus( HttpStatus.OK )
    public void deleteProduct( final @PathVariable
    Long productId ) {

        _productService.deleteProduct( productId );
    }
    
}
