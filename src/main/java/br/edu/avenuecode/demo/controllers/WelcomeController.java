/*****************************************************************************
 * Project:   <B>AvenueTestDemo</B>
 * File:      <I>WelcomeController.java</I>
 * Author:    <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * Creation:  Jan 18, 2018
 * --------------------------------------------------------------------------
 * <I>Copyright 2018, <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * All rights are reserved. Reproduction in whole or part is prohibited
 * without the written consent of the copyright owner.</I>
 *****************************************************************************/
package br.edu.avenuecode.demo.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * TODO Class documentation comment here
 *
 * @author hugo
 * @created Jan 18, 2018
 * @since JDK 1.8
 */
@Controller
public class WelcomeController {
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index() {
        return "index";
    }
    
}
