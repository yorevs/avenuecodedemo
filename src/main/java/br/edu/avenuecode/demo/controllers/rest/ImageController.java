/*****************************************************************************
 * Project: <B>demo</B>
 * File: <I>ImageController.java</I>
 * Author: <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * Creation: Jan 15, 2018
 * --------------------------------------------------------------------------
 * <I>Copyright 2018, <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * All rights are reserved. Reproduction in whole or part is prohibited
 * without the written consent of the copyright owner.</I>
 *****************************************************************************/

package br.edu.avenuecode.demo.controllers.rest;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.edu.avenuecode.demo.models.Image;
import br.edu.avenuecode.demo.models.Product;
import br.edu.avenuecode.demo.services.ImageService;
import br.edu.avenuecode.demo.services.ProductService;

/**
 * Class responsible to customize the REST services related to Images(entity)
 *
* @author <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * @created Jan 15, 2018
 * @since JDK 1.8
 */
@RestController
public class ImageController {

    @Autowired
    private ImageService _imageService;

    @Autowired
    private ProductService _productService;

    // ------------------------------------------------------------------------
    // READ operations
    // ------------------------------------------------------------------------

    /**
     * A. Get all images regardless of any parent product
     * - URL: /images
     * - Method: GET
     * - Headers: [Content-Type] = application/json
     * - Body: empty
     *
     * @return JSON text representing an array of all system images
     * @since JDK 1.8
     */
    @RequestMapping( method = RequestMethod.GET, value = "/images"/* , headers = { "content-type=application/json" } */ )
    @ResponseStatus( HttpStatus.OK )
    public List<Image> getAllImages() {
        
        return _imageService.readAllImages();
    }

    /**
     * B. Get one image based on a specified ID
     * - URL: /images/{imageId}
     * - Method: GET
     * - Headers: [Content-Type] = application/json
     * - Body: empty
     *
     * @param imageId
     * @return JSON text representing the specified image
     * @since JDK 1.8
     */
    @RequestMapping( method = RequestMethod.GET, value = "/images/{imageId}"/* , headers = { "content-type=application/json" } */ )
    @ResponseStatus( HttpStatus.OK )
    public Image getImageById( final @PathVariable
    Long imageId ) {
        
        return _imageService.readImage( imageId );
    }
    
    /**
     * C. Get all images related to a specified parent product
     * - URL: /products/{productId}/images
     * - Method: GET
     * - Headers: [Content-Type] = application/json
     * - Body: empty
     *
     * @param productId
     * @return JSON text representing an array of all product related images
     * @since JDK 1.8
     */
    @RequestMapping( method = RequestMethod.GET, value = "/products/{productId}/images"/* , headers = { "content-type=application/json" } */ )
    @ResponseStatus( HttpStatus.OK )
    public List<Image> getAllProductImages( final @PathVariable
    Long productId ) {
        
        return _imageService.readAllProductImages( productId );
    }

    /**
     * D. Get a specific image of a specified parent product
     * - URL: /products/{productId}/images/{imageId}
     * - Method: GET
     * - Headers: [Content-Type] = application/json
     * - Body: empty
     *
     * @param productId
     * @param imageId
     * @return JSON text representing the specified image of the specified
     *         parent product
     * @since JDK 1.8
     */
    @RequestMapping( method = RequestMethod.GET, value = "/products/{productId}/images/{imageId}"/* , headers = { "content-type=application/json" } */ )
    @ResponseStatus( HttpStatus.OK )
    public Image getProductImageById( final @PathVariable
    Long productId, final @PathVariable
    Long imageId ) {
        
        return _imageService.readProductImageById( productId, imageId );
    }

    // ------------------------------------------------------------------------
    // CREATE operations
    // ------------------------------------------------------------------------
    
    /**
     * A. Create a new image regardless of any parent product
     * - URL: /images
     * - Method: POST
     * - Headers: [Content-Type] = application/json
     * - Body:
     * {
     * "type": "<image_name>",
     * "product": <parent_project>
     * }
     *
     * @param image
     * @since JDK 1.8
     */
    @RequestMapping( method = RequestMethod.POST, value = "/images"/* , headers = { "content-type=application/json" } */ )
    @ResponseStatus( HttpStatus.CREATED )
    public void createImage( final @RequestBody
    Image image ) {
        
        _imageService.createImage( image );
    }

    /**
     * B. Create a new image for a specified parent product
     * - URL: /products/{productId}/images
     * - Method: POST
     * - Headers: [Content-Type] = application/json
     * - Body:
     * {
     * "type": "<image_name>"
     * }
     *
     * @param image
     * @since JDK 1.8
     */
    @RequestMapping( method = RequestMethod.POST, value = "/products/{productId}/images"/* , headers = { "content-type=application/json" } */ )
    @ResponseStatus( HttpStatus.CREATED )
    public void createProductImage( final @RequestBody
    Image image, final @PathVariable
    Long productId ) {

        Product parentProduct = _productService.readProduct( productId );
        image.setProduct( parentProduct );
        _imageService.createImage( image );
    }

    // ------------------------------------------------------------------------
    // UPDATE operations
    // ------------------------------------------------------------------------

    /**
     * A. Update an existing image
     * - URL: /image/{imageId}
     * - Method: PUT
     * - Headers: [Content-Type] = application/json
     * - Body:
     * {
     * "type": "<upd_ image_type>"
     * }
     *
     * @param image
     * @param id
     * @since JDK 1.8
     */
    @RequestMapping( method = RequestMethod.PUT, value = "/images/{imageId}"/* , headers = { "content-type=application/json" } */ )
    @ResponseStatus( HttpStatus.OK )
    public void updateImage( final @RequestBody
    Image image, final @PathVariable
    Long imageId ) {

        image.setId( imageId );
        _imageService.updateImage( image );
    }

    /**
     * B. Associate an existing image to a product
     * - URL: /products/{productId}/images
     * - Method: PUT
     * - Headers: [Content-Type] = text/uri-list
     * - Body:
     * <image1_url>
     * <image2_url>
     *
     * @param imageUrls
     * @param productId
     * @since JDK 1.8
     */
    @RequestMapping( method = RequestMethod.PUT, value = "/products/{productId}/images", headers = { "content-type=text/uri-list" } )
    @ResponseStatus( HttpStatus.OK )
    public void updateProjectImage( final @RequestBody
    String imageUrls, final @PathVariable
    Long productId ) {

        List<String> urlList = Arrays.asList( imageUrls.split( "\n" ) );
        Product product = _productService.readProduct( productId );

        urlList.forEach( u -> {
            
            String idRef = u.substring( u.lastIndexOf( '/' ) + 1 );
            
            if ( idRef != null && idRef.matches( "[0-9]*" ) ) {
                
                Long imageId = Long.parseLong( idRef );
                Image refImage = _imageService.readImage( imageId );
                
                if ( refImage != null ) {
                    
                    refImage.setProduct( product );
                    _imageService.updateImage( refImage );
                }
            }
        } );
    }

    // ------------------------------------------------------------------------
    // DELETE operations
    // ------------------------------------------------------------------------

    /**
     * A. Delete an existing image
     * - URL: /images/{imageId}
     * - Method: DELETE
     * - Headers: empty
     * - Body: empty
     *
     * @param id
     * @since JDK 1.8
     */
    @RequestMapping( method = RequestMethod.DELETE, value = "/images/{imageId}" )
    @ResponseStatus( HttpStatus.OK )
    public void deleteImage( final @PathVariable
    Long imageId ) {
        
        _imageService.deleteImage( imageId );
    }
}
