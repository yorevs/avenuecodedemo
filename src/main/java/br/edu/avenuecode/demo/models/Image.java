/*****************************************************************************
 * Project: <B>demo</B>
 * File: <I>Image.java</I>
 * Author: <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * Creation: Jan 15, 2018
 * --------------------------------------------------------------------------
 * <I>Copyright 2018, <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * All rights are reserved. Reproduction in whole or part is prohibited
 * without the written consent of the copyright owner.</I>
 *****************************************************************************/

package br.edu.avenuecode.demo.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

/**
 * The entity Image. A simple POJO class plus annotations regarding it's
 * Database table.
 *
 * @author <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * @created Jan 15, 2018
 * @since JDK 1.8
 */
@Entity
@Table(name="IMAGES")
public class Image {
    
    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;

    @Column( nullable = false )
    private String type;

    @JsonBackReference
    @ManyToOne
    @JoinColumn( name = "product_id" )
    private Product product;

    public Image( final String type ) {

        this.type = type;
    }
    
    public Image() {

        super();
    }
    
    /**
     * Getter for member id
     *
     * @return the id
     */
    public final Long getId() {

        return id;
    }
    
    /**
     * Setter for member id
     *
     * @param id the id to set
     */
    public final void setId( final Long id ) {

        this.id = id;
    }
    
    /**
     * Getter for member type
     *
     * @return the type
     */
    public final String getType() {

        return type;
    }
    
    /**
     * Setter for member type
     *
     * @param type the type to set
     */
    public final void setType( final String type ) {

        this.type = type;
    }
    
    /**
     * Getter for member product
     *
     * @return the product
     */
    public final Product getProduct() {

        return product;
    }
    
    /**
     * Setter for member product
     *
     * @param product the product to set
     */
    public final void setProduct( final Product product ) {

        this.product = product;
    }

    @Override
    public String toString() {
        
        return String.format( "Id: %d, Type: \"%s\" %s", id, type, ( product != null ? ", Product: \"%s\"" + product.getName() : "" ) );
    }
    
}
