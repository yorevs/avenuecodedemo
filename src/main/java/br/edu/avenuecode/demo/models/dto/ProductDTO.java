/*****************************************************************************
 * Project: <B>AvenueTestDemo</B>
 * File: <I>ProductDTO.java</I>
 * Author: <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * Creation: Jan 17, 2018
 * --------------------------------------------------------------------------
 * <I>Copyright 2018, <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * All rights are reserved. Reproduction in whole or part is prohibited
 * without the written consent of the copyright owner.</I>
 *****************************************************************************/

package br.edu.avenuecode.demo.models.dto;

/**
 * DTO for Product entity. Represents a different JSON output for the Product
 * entity
 *
* @author <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * @created Jan 17, 2018
 * @since JDK 1.8
 */
public class ProductDTO {
    
    private Long id;
    
    private String name;

    private String description;

    /**
     * Creates a ProductDTO instance
     */
    public ProductDTO() {}

    public ProductDTO( final Long id, final String name, final String description ) {
        
        super();
        this.id = id;
        this.name = name;
        this.description = description;
    }

    /**
     * Getter for member id
     *
     * @return the id
     */
    public final Long getId() {
        
        return id;
    }

    /**
     * Setter for member id
     *
     * @param id the id to set
     */
    public final void setId( final Long id ) {
        
        this.id = id;
    }

    /**
     * Getter for member name
     *
     * @return the name
     */
    public final String getName() {
        
        return name;
    }

    /**
     * Setter for member name
     *
     * @param name the name to set
     */
    public final void setName( final String name ) {
        
        this.name = name;
    }

    /**
     * Getter for member description
     *
     * @return the description
     */
    public final String getDescription() {
        
        return description;
    }

    /**
     * Setter for member description
     *
     * @param description the description to set
     */
    public final void setDescription( final String description ) {
        
        this.description = description;
    }

}
