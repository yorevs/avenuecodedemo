/*****************************************************************************
 * Project: <B>demo</B>
 * File: <I>Product.java</I>
 * Author: <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * Creation: Jan 15, 2018
 * --------------------------------------------------------------------------
 * <I>Copyright 2018, <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * All rights are reserved. Reproduction in whole or part is prohibited
 * without the written consent of the copyright owner.</I>
 *****************************************************************************/

package br.edu.avenuecode.demo.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

/**
 * The entity Product. A simple POJO class plus annotations regarding it's Database table.
 *
* @author <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * @created Jan 15, 2018
 * @since JDK 1.8
 */
@Entity
@Table(name="PRODUCTS")
public class Product {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    private Long id;
    
    @Column( nullable = false )
    private String name;
    
    @Column
    private String description;

    @JsonManagedReference
    @OneToMany( mappedBy = "product", cascade = CascadeType.ALL, fetch = FetchType.LAZY )
    private List<Image> images;
    
    @JsonManagedReference
    @OneToMany( mappedBy = "parent", cascade = CascadeType.ALL, fetch = FetchType.LAZY )
    private List<Product> products;
    
    @JsonBackReference
    @ManyToOne
    @JoinColumn( name = "parent_id" )
    private Product parent;

    public Product() {
        
        super();
    }

    public Product( final String name, final String description ) {

        super();
        this.name = name;
        this.description = description;
    }

    public Product( final String name, final String description, final List<Image> images, final List<Product> products ) {

        this.name = name;
        this.description = description;
        this.images = images;
    }
    
    /**
     * Getter for member id
     *
     * @return the id
     */
    public final Long getId() {
        
        return id;
    }

    /**
     * Setter for member id
     *
     * @param id the id to set
     */
    public final void setId( final Long id ) {
        
        this.id = id;
    }

    /**
     * Getter for member name
     *
     * @return the name
     */
    public final String getName() {
        
        return name;
    }

    /**
     * Setter for member name
     *
     * @param name the name to set
     */
    public final void setName( final String name ) {
        
        this.name = name;
    }

    /**
     * Getter for member description
     *
     * @return the description
     */
    public final String getDescription() {
        
        return description;
    }

    /**
     * Setter for member description
     *
     * @param description the description to set
     */
    public final void setDescription( final String description ) {
        
        this.description = description;
    }

    /**
     * Getter for member images
     *
     * @return the images
     */
    public final List<Image> getImages() {
        
        return images;
    }

    /**
     * Setter for member images
     *
     * @param images the images to set
     */
    public final void setImages( final List<Image> images ) {
        
        this.images = images;
    }

    /**
     * Getter for member products
     *
     * @return the products
     */
    public final List<Product> getProducts() {
        
        return products;
    }

    /**
     * Setter for member products
     *
     * @param products the products to set
     */
    public final void setProducts( final List<Product> products ) {
        
        this.products = products;
    }

    /**
     * Getter for member parent
     *
     * @return the parent
     */
    public final Product getParent() {
        
        return parent;
    }

    /**
     * Setter for member parent
     *
     * @param parent the parent to set
     */
    public final void setParent( final Product parent ) {
        
        this.parent = parent;
    }

    @Override
    public String toString() {

        return String.format( "Id: %d, Name: \"%s\", Description: \"%s\", Images: %s, Products: %s", id, name, description, images, products );
    }
}
