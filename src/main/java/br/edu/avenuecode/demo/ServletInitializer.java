/*****************************************************************************
 * Project:   <B>demo</B>
 * File:      <I>ServletInitializer.java</I>
 * Author:    <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * Creation:  Jan 15, 2018
 * --------------------------------------------------------------------------
 * <I>Copyright 2018, <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * All rights are reserved. Reproduction in whole or part is prohibited
 * without the written consent of the copyright owner.</I>
 *****************************************************************************/
package br.edu.avenuecode.demo;

import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * Spring Boot servlet initializer
 *
 * @author hugo
 * @created Jan 18, 2018
 * @since JDK 1.8
 */
public class ServletInitializer extends SpringBootServletInitializer {
    
    @Override
    protected SpringApplicationBuilder configure( SpringApplicationBuilder application ) {
        
        return application.sources( DemoApplication.class );
    }
    
}
