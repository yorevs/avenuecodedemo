(function() {
    "use strict";
    
    var app = angular.module("AvenueCodeApp", []);
    
    app.service("AppDataSrv", function ($http) {
        
        var self = this;
        
        self.serverPort = 8080;
        self.serverAddress = 'http://localhost:' + self.serverPort + '/';
        
        self.getProducts = function() {
            
            var promise1 = $http.get(self.serverAddress + "/products/dto");
            var promise2 = promise1.then(function(response) {
                console.log("Read all products from server: Response(" + response.status + ")");
                return response.data;
            });

            return promise2;
        };
        
        self.getImages = function() {
            
            var promise1 = $http.get(self.serverAddress + "/images");
            var promise2 = promise1.then(function(response) {
                console.log("Read all images from server: Response(" + response.status + ")");
                return response.data;
            });

            return promise2;
        };
        
        self.createProduct = function(productData) {
            
            var promise1 = $http.post(self.serverAddress + "/products", productData);
            var promise2 = promise1.then(function(response) {
                console.log("Create product from server: Response(" + response.status + ")");
                return response.data;
            });
            
            return promise2;
        };
        
        self.createImage = function(imageData) {
            
            var promise1 = $http.post(self.serverAddress + "/images", imageData);
            var promise2 = promise1.then(function(response) {
                console.log("Create image from server: Response(" + response.status + ")");
                return response.data;
            });
            
            return promise2;
        };
        
        self.updateProduct = function(productData) {
            
            var promise1 = $http.put(self.serverAddress + "/products/" + productData.id, productData);
            var promise2 = promise1.then(function(response) {
                console.log("Update product from server: Response(" + response.status + ")");
                return response.data;
            });
            
            return promise2;
        };
        
        self.updateImage = function(imageData) {
            
            var promise1 = $http.put(self.serverAddress + "/images/" + imageData.id, imageData);
            var promise2 = promise1.then(function(response) {
                console.log("Update image from server: Response(" + response.status + ")");
                return response.data;
            });
            
            return promise2;
        };
        
        self.deleteProduct = function(productData) {
            
            var promise1 = $http.delete(self.serverAddress + "/products/" + productData.id);
            var promise2 = promise1.then(function(response) {
                console.log("Delete product from server: Response(" + response.status + ")");
                return response.data;
            });
            
            return promise2;
        };
        
        self.deleteImage = function(imageData) {
            
            var promise1 = $http.delete(self.serverAddress + "/images/" + imageData.id);
            var promise2 = promise1.then(function(response) {
                console.log("Delete image from server: Response(" + response.status + ")");
                return response.data;
            });
            
            return promise2;
        };
        
        console.log('AppDataSrv loaded');
        
    });
    
})();