(function() {
    "use strict";
    
    var app = angular.module("AvenueCodeApp");
    
    app.controller("AvenueCodeCtrl", function(AppDataSrv, $scope, $window, $timeout, $location, $anchorScroll) {
        
        var self = this;
        
        self.selected = "products";
        self.newProduct = {};
        self.allProducts = [];
        self.allImages = [];
        
        self.select = function(itemName) {
            
            if ( itemName !== self.selected ) {
                
                self.selected = itemName;
                $window.sessionStorage.setItem('AvenueCodeApp.Cache.selectedItem', itemName);
                
                if ( $scope.newImage ) {
                    
                    delete $scope.newImage;
                    if ( self.createMode ) {
                        self.allImages.pop();
                    }
                }
                if ( $scope.newProduct ) {
                    delete $scope.newProduct;
                    if ( self.createMode ) {
                        self.allProducts.pop();
                    }
                }
                
                self.cleanup();
                console.log('item selected: ' + itemName);
            }
        };
        
        self.cleanup = function() {
            
            self.createMode = false;
            self.editMode = false;
            $location.hash('home');
            
            console.log('cleanup called');
        };
        
        self.setErrorMessage = function(errorMessage) {
            
            self.errorMessage = errorMessage;
            self.successMessage = null;
            $window.sessionStorage.setItem('AvenueCodeApp.Cache.errorMessage', errorMessage);
        };
        
        self.setSuccessMessage = function(successMessage) {
            
            self.errorMessage = null;
            self.successMessage = successMessage;
            $window.sessionStorage.setItem('AvenueCodeApp.Cache.successMessage', successMessage);
        };
        
        self.addProduct = function(product) {
            
            $scope.newProduct = {
                "id": null,
                "name": null,
                "description": null
            };
            
            self.createMode = true;
            self.allProducts.push($scope.newProduct);
            $location.hash('createProduct');
            $anchorScroll();
        };
        
        self.addImage = function(image) {
            
            $scope.newImage = {
                "id": null,
                "type": null
            };
            
            self.createMode = true;
            self.allImages.push($scope.newImage);
            $location.hash('createImage');
            $anchorScroll();
        };
        
        self.editProduct = function(product) {
            
            self.editMode = true;
            $scope.newProduct = product;
        };
        
        self.editImage = function(image) {
            
            self.editMode = true;
            $scope.newImage= image;
        };
        
        self.cancelProduct = function() {
            
            if ( self.createMode ) {
                
                delete $scope.newProduct;
                self.allProducts.pop();
            }
            
            self.cleanup();
        };
        
        self.cancelImage = function() {
            
            if ( self.createMode ) {
                
                delete $scope.newImage;
                self.allImages.pop();
            }
            
            self.cleanup();
        };
        
        self.removeProduct = function(product) {
            
            AppDataSrv.deleteProduct(product)
                .then(
                    function(data) {
                        self.setSuccessMessage( 'Successfully deleted product' );
                        console.log('Successfully deleted product: "' + product.name + '"');
                        var index = self.allProducts.indexOf(product);
                        self.allProducts.splice(index, 1);
                        self.delayedCleanup();
                    },
                    function(data) {
                        self.setErrorMessage( 'Failed to delete product' );
                        console.log('Failed to delete product: "' + product.name + '"');
                        console.log(data);
                        self.delayedCleanup();
                    }
                );
        };
        
        self.removeImage = function(image) {
            
            AppDataSrv.deleteImage(image)
                .then(
                    function(data) {
                        self.setSuccessMessage( 'Successfully deleted image' );
                        console.log('Successfully deleted image: "' + image.name + '"');
                        var index = self.allImages.indexOf(image);
                        self.allImages.splice(index, 1);
                        self.delayedCleanup();
                    },
                    function(data) {
                        self.setErrorMessage( 'Failed to delete image' );
                        console.log('Failed to delete image: "' + image.name + '"');
                        console.log(data);
                        self.delayedCleanup();
                    }
                );
        };
        
        self.saveProduct = function() {
            
            if ( self.createMode ) {
                
                delete $scope.newProduct.id;
                
                AppDataSrv.createProduct($scope.newProduct)
                    .then(
                        function(data) {
                            self.setSuccessMessage( 'Successfully saved product' );
                            console.log('Successfully saved product: "' + $scope.newProduct.name + '"');
                            $window.location.reload();
                        },
                        function(data) {
                            self.setErrorMessage( 'Failed to save product' );
                            console.log('Failed to save product: "' + $scope.newProduct.name + '"');
                            console.log(data);
                            delete $scope.newProduct;
                            self.allProducts.pop();
                            self.delayedCleanup();
                        }
                    );
            }
            else {
                
                AppDataSrv.updateProduct($scope.newProduct)
                    .then(
                        function(data) {
                            self.setSuccessMessage( 'Successfully updated product' );
                            console.log('Successfully updated product: "' + $scope.newProduct.name + '"');
                            self.delayedCleanup();
                        },
                        function(data) {
                            self.setErrorMessage( 'Failed to updated product' );
                            console.log('Failed to updated product: "' + $scope.newProduct.name + '"');
                            console.log(data);
                            self.delayedCleanup();
                        }
                    );
            }
            
            self.createMode = false;
            self.editMode = false;
        };
        
        self.saveImage = function() {
            
            if ( self.createMode ) {
                
                delete $scope.newImage.id;
                
                AppDataSrv.createImage($scope.newImage)
                    .then(
                        function(data) {
                            self.setSuccessMessage( 'Successfully saved image' );
                            console.log('Successfully saved image: "' + $scope.newImage.type + '"');
                            $window.location.reload();
                        },
                        function(data) {
                            self.setErrorMessage( 'Failed to save image' );
                            console.log('Failed to save image: "' + $scope.newImage.type + '"');
                            console.log(data);
                            delete $scope.newImage;
                            self.allImages.pop();
                            self.delayedCleanup();
                        }
                    );
            }
            else {
                
                AppDataSrv.updateImage($scope.newImage)
                    .then(
                        function(data) {
                            self.setSuccessMessage( 'Successfully updated image' );
                            console.log('Successfully updated image: "' + $scope.newImage.name + '"');
                            self.delayedCleanup();
                        },
                        function(data) {
                            self.setErrorMessage( 'Failed to updated image' );
                            console.log('Failed to updated image: "' + $scope.newImage.name + '"');
                            console.log(data);
                            self.delayedCleanup();
                        }
                    );
            }
            
            self.createMode = false;
            self.editMode = false;
        };
        
        AppDataSrv.getProducts()
            .then(
                function(data) {
                    self.allProducts = data;
                    console.log('Successfully retrieved server products: [' + data.length + ']');
                    console.log(data);
                    self.delayedCleanup();
                },
                function(data) {
                    console.log('Failed to retrieve server products');
                    console.log(data);
                    self.delayedCleanup();
                }
            );
        
        AppDataSrv.getImages()
            .then(
                function(data) {
                    self.allImages = data;
                    console.log('Successfully retrieved server images: [' + data.length + ']');
                    console.log(data);
                    self.delayedCleanup();
                },
                function(data) {
                    console.log('Failed to retrieve server images');
                    console.log(data);
                    self.delayedCleanup();
                }
            );
        
        self.restoreCache = function() {
            
            var em = $window.sessionStorage.getItem('AvenueCodeApp.Cache.errorMessage');
            var sm = $window.sessionStorage.getItem('AvenueCodeApp.Cache.successMessage');
            var sl = $window.sessionStorage.getItem('AvenueCodeApp.Cache.selectedItem');
            
            console.log(em);
            console.log(sm);
            console.log(sl);
            
            self.successMessage = ( sm && sm !== 'null' ? sm : null );
            self.errorMessage = ( em && em !== 'null' ? em : null );
            self.select( sl && sl !== 'null' ? sl : 'products' );
            
            self.delayedCleanup();
            
            console.log('Cache restored');
        };
        
        angular.element(document).ready(function () {
            
            $timeout( function() {
                self.restoreCache();
            }, 0 );
        });
        
        self.delayedCleanup = function() {
            
            $timeout( function() {
                self.setSuccessMessage(null);
                self.setErrorMessage(null);
            }, 2500 );
        };
        
        console.log("AvenueCodeCtrl loaded");
        
    });
    
})();