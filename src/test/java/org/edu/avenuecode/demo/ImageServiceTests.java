/*****************************************************************************
 * Project:   <B>AvenueTestDemo</B>
 * File:      <I>ImageServiceTests.java</I>
 * Author:    <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * Creation:  Jan 18, 2018
 * --------------------------------------------------------------------------
 * <I>Copyright 2018, <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * All rights are reserved. Reproduction in whole or part is prohibited
 * without the written consent of the copyright owner.</I>
 *****************************************************************************/
package org.edu.avenuecode.demo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import br.edu.avenuecode.demo.DemoApplication;
import br.edu.avenuecode.demo.models.Image;
import br.edu.avenuecode.demo.models.Product;
import br.edu.avenuecode.demo.services.ImageService;
import br.edu.avenuecode.demo.services.ProductService;


/**
 * Set of TestCases for the Database Services:: ImageServices
 *
 * @author hugo
 * @created Jan 18, 2018
 * @since JDK 1.8
 */
@RunWith( SpringRunner.class )
@SpringBootTest(classes = DemoApplication.class, webEnvironment = WebEnvironment.DEFINED_PORT)
public class ImageServiceTests {
    
    @Autowired
    ImageService imageService;
    
    @Autowired
    ProductService productService;

    private Product shoes;
    
    private Image accessories;

    private Image clothes;
    
    private static final String SHOES = "Shoes";
    private static final String SHOES_DESC = "Description for shoes";
    private static final String ACCESSORIES = "Accessories";
    private static final String CLOTHES = "Clothes";
    
    @Before
    public void setUp() {
        
        accessories = new Image(ACCESSORIES);
        imageService.createImage( accessories );
        accessories.setId( TestUtils.nextImageId() );
        clothes = new Image(CLOTHES);
        imageService.createImage( clothes );
        clothes.setId( TestUtils.nextImageId() );
    }
    
    @After
    public void tearDown() {
        
        imageService.deleteImage( clothes.getId() );
        imageService.deleteImage( accessories.getId() );
        
        if ( shoes != null ) {
            
            productService.deleteProduct( shoes.getId() );
            shoes = null;
        }
    }
    
    /**
     * Test method for {@link br.edu.avenuecode.demo.services.ImageService#readAllImages()}.
     */
    @Test
    public void testReadAllImages() {
        
        List<Image> allImages = imageService.readAllImages();
        assertNotNull(allImages);
        assertEquals( "There are different number of images", 2, allImages.size() );
        allImages.stream().anyMatch( p -> p.getType().equals(accessories.getType()) || (p.getType().equals(clothes.getType())) );

    }
    
    /**
     * Test method for {@link br.edu.avenuecode.demo.services.ImageService#readImage(java.lang.Long)}.
     */
    @Test
    public void testReadImage() {
        
        Image image = imageService.readImage( accessories.getId() );
        assertNotNull(image);
        assertEquals( "Image is different", ACCESSORIES, image.getType() );
    }
    
    /**
     * Test method for {@link br.edu.avenuecode.demo.services.ImageService#readAllProductImages(java.lang.Long)}.
     */
    @Test
    public void testReadAllProductImages() {
        
        shoes = new Product(SHOES, SHOES_DESC);
        productService.createProduct( shoes );
        shoes.setId( TestUtils.nextProductId() );
        accessories.setProduct( shoes );
        imageService.updateImage( accessories );
        clothes.setProduct( shoes );
        imageService.updateImage( clothes );
        
        List<Image> productImages = imageService.readAllProductImages( shoes.getId() );
        assertNotNull(productImages);
        assertTrue( productImages.stream().allMatch( i -> i.getType().equals(accessories.getType()) || i.getType().equals(clothes.getType()) ) );
    }
    
    /**
     * Test method for {@link br.edu.avenuecode.demo.services.ImageService#readProductImageById(java.lang.Long, java.lang.Long)}.
     */
    @Test
    public void testReadProductImageById() {
        
        shoes = new Product(SHOES, SHOES_DESC);
        productService.createProduct( shoes );
        shoes.setId( TestUtils.nextProductId() );
        accessories.setProduct( shoes );
        imageService.updateImage( accessories );
        clothes.setProduct( shoes );
        imageService.updateImage( clothes );
        
        Image image = imageService.readProductImageById( shoes.getId(), clothes.getId() );
        assertNotNull(image);
        assertEquals( "Image is different", CLOTHES, image.getType() );
    }
    
    /**
     * Test method for {@link br.edu.avenuecode.demo.services.ImageService#createImage(br.edu.avenuecode.demo.models.Image)}.
     */
    @Test
    public void testCreateImage() {
        
        Image trinket = new Image( "Trinkets" );
        imageService.createImage( trinket );
        trinket.setId( TestUtils.nextImageId() );
        List<Image> allImages = imageService.readAllImages(); 
        assertEquals( "There are different number of images", 3, allImages.size() );
        allImages.stream().anyMatch( p -> p.getType().equals(trinket.getType()) );
        imageService.deleteImage( trinket.getId() );
    }
    
    /**
     * Test method for {@link br.edu.avenuecode.demo.services.ImageService#updateImage(br.edu.avenuecode.demo.models.Image)}.
     */
    @Test
    public void testUpdateImage() {
        
        accessories.setType( "Accessories-Women" );
        imageService.updateImage( accessories );
        Image image = imageService.readImage( accessories.getId() );
        assertNotNull(image);
        assertEquals( "Image is different", "Accessories-Women", image.getType() );
        assertEquals( "Image is different", accessories.getId(), image.getId() );
    }
    
    /**
     * Test method for {@link br.edu.avenuecode.demo.services.ImageService#deleteImage(java.lang.Long)}.
     */
    @Test
    public void testDeleteImage() {
        
        Image trinket = new Image( "Trinket" );
        imageService.createImage( trinket );
        trinket.setId( TestUtils.nextImageId() );
        Image Image = imageService.readImage( trinket.getId() );
        assertNotNull(Image);
        assertEquals( "Image is different", "Trinket", Image.getType() );
        imageService.deleteImage( Image.getId() );
        Image ref = imageService.readImage( trinket.getId() );
        assertNull(ref);
    }
    
}
