/*****************************************************************************
 * Project:   <B>AvenueTestDemo</B>
 * File:      <I>ProductServiceTests.java</I>
 * Author:    <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * Creation:  Jan 18, 2018
 * --------------------------------------------------------------------------
 * <I>Copyright 2018, <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * All rights are reserved. Reproduction in whole or part is prohibited
 * without the written consent of the copyright owner.</I>
 *****************************************************************************/
package org.edu.avenuecode.demo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

import br.edu.avenuecode.demo.DemoApplication;
import br.edu.avenuecode.demo.models.Product;
import br.edu.avenuecode.demo.services.ProductService;


/**
 * Set of TestCases for the Database Services:: ProductServices
 *
* @author <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * @created Jan 17, 2018
 * @since JDK 1.8
 */
@RunWith( SpringRunner.class )
@SpringBootTest(classes = DemoApplication.class, webEnvironment = WebEnvironment.DEFINED_PORT)
public class ProductServiceTests {
    
    @Autowired
    ProductService productService;

    private Product shoes;

    private Product hats;
    
    private static final String SHOES = "Shoes";
    private static final String SHOES_DESC = "Description for shoes";
    private static final String HATS = "Hats";
    private static final String HATS_DESC = "Description for hats";
    
    @Before
    public void setUp() {
        
        shoes = new Product(SHOES, SHOES_DESC);
        productService.createProduct( shoes );
        shoes.setId( TestUtils.nextProductId() );
        hats = new Product(HATS, HATS_DESC);
        productService.createProduct( hats );
        hats.setId( TestUtils.nextProductId() );
    }
    
    @After
    public void tearDown() {
        
        productService.deleteProduct( hats.getId() );
        productService.deleteProduct( shoes.getId() );
    }
    
    /**
     * Test method for {@link br.edu.avenuecode.demo.services.ProductService#readAllProducts()}.
     */
    @Test
    public void testReadAllProducts() {
        
        List<Product> allProducts = productService.readAllProducts();
        assertNotNull(allProducts);
        assertEquals( "There are different number of products", 2, allProducts.size() );
        allProducts.stream().anyMatch( p -> p.getName().equals(shoes.getName()) || (p.getName().equals(hats.getName())) );
    }
    
    /**
     * Test method for {@link br.edu.avenuecode.demo.services.ProductService#readProduct(java.lang.Long)}.
     */
    @Test
    public void testReadProduct() {
        
        Product product = productService.readProduct( shoes.getId() );
        assertNotNull(product);
        assertEquals( "Product is different", SHOES, product.getName() );
        assertEquals( "Product is different", SHOES_DESC, product.getDescription() );
    }
    
    /**
     * Test method for {@link br.edu.avenuecode.demo.services.ProductService#readParentProducts(java.lang.Long)}.
     */
    @Test
    public void testReadParentProducts() {
        
        hats.setParent( shoes );
        productService.updateProduct( hats );
        List<Product> parentProducts = productService.readParentProducts( shoes.getId() );
        assertNotNull(parentProducts);
        assertTrue( parentProducts.stream().allMatch( p -> p.getName().equals(hats.getName())) );
    }
    
    /**
     * Test method for {@link br.edu.avenuecode.demo.services.ProductService#createProduct(br.edu.avenuecode.demo.models.Product)}.
     */
    @Test
    public void testCreateProduct() {
        
        Product ball = new Product( "Ball", "Ball description" );
        ball.setId( TestUtils.nextProductId() );
        productService.createProduct( ball );
        List<Product> allProducts = productService.readAllProducts(); 
        assertEquals( "There are different number of products", 3, allProducts.size() );
        allProducts.stream().anyMatch( p -> p.getName().equals(ball.getName()) && p.getDescription().equals(ball.getDescription()) );
        productService.deleteProduct( ball.getId() );
    }
    
    /**
     * Test method for {@link br.edu.avenuecode.demo.services.ProductService#updateProduct(br.edu.avenuecode.demo.models.Product)}.
     */
    @Test
    public void testUpdateProduct() {
        
        Product updShoes = new Product( "Red Shoes", "RED Shoes description" );
        shoes.setName( updShoes.getName() );
        shoes.setDescription( updShoes.getDescription() );
        productService.updateProduct( shoes );
        Product product = productService.readProduct( shoes.getId() );
        assertNotNull(product);
        assertEquals( "Product is different", "Red Shoes", product.getName() );
        assertEquals( "Product is different", "RED Shoes description", product.getDescription() );
        assertEquals( "Product is different", shoes.getId(), product.getId() );
    }
    
    /**
     * Test method for {@link br.edu.avenuecode.demo.services.ProductService#deleteProduct(java.lang.Long)}.
     */
    @Test
    public void testDeleteProduct() {
        
        Product ball = new Product( "Ball", "Ball description" );
        ball.setId( TestUtils.nextProductId() );
        productService.createProduct( ball );
        Product product = productService.readProduct( ball.getId() );
        assertNotNull(product);
        assertEquals( "Product is different", "Ball", product.getName() );
        assertEquals( "Product is different", "Ball description", product.getDescription() );
        productService.deleteProduct( product.getId() );
        Product ref = productService.readProduct( ball.getId() );
        assertNull(ref);
    }
    
}
