/*****************************************************************************
 * Project: <B>AvenueTestDemo</B>
 * File: <I>TestUtils.java</I>
 * Author: <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * Creation: Jan 18, 2018
 * --------------------------------------------------------------------------
 * <I>Copyright 2018, <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * All rights are reserved. Reproduction in whole or part is prohibited
 * without the written consent of the copyright owner.</I>
 *****************************************************************************/

package org.edu.avenuecode.demo;

import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

/**
 * TODO Class documentation comment here
 *
 * @author hugo
 * @created Jan 18, 2018
 * @since JDK 1.8
 */
public class TestUtils {

    private static long nextProductId = 1;

    private static long nextImageId = 1;

    static long nextProductId() {

        return TestUtils.nextProductId++;
    }

    static long nextImageId() {

        return TestUtils.nextImageId++;
    }

    public static <T> ResponseEntity<T> doGetRequest( final TestRestTemplate template, final String url, final HttpHeaders headers, final Class<T> responseType ) {

        HttpEntity<?> entity = new HttpEntity<>( null, headers );
        return template.exchange( url, HttpMethod.GET, entity, responseType );
    }

    public static <T> ResponseEntity<T> doPostRequest( final TestRestTemplate template, final String url, final HttpHeaders headers, final Object body, final Class<T> responseType ) {

        HttpEntity<?> entity = new HttpEntity<>( body, headers );
        return template.exchange( url, HttpMethod.POST, entity, responseType );
    }

    public static <T> ResponseEntity<T> doPutRequest( final TestRestTemplate template, final String url, final HttpHeaders headers, final Object body, final Class<T> responseType ) {

        HttpEntity<?> entity = new HttpEntity<>( body, headers );
        return template.exchange( url, HttpMethod.PUT, entity, responseType );
    }
}
