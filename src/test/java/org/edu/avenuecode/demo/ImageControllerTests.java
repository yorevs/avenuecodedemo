/*****************************************************************************
 * Project:   <B>AvenueTestDemo</B>
 * File:      <I>ImageControllerTests.java</I>
 * Author:    <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * Creation:  Jan 17, 2018
 * --------------------------------------------------------------------------
 * <I>Copyright 2018, <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * All rights are reserved. Reproduction in whole or part is prohibited
 * without the written consent of the copyright owner.</I>
 *****************************************************************************/
package org.edu.avenuecode.demo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.json.JSONException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import br.edu.avenuecode.demo.DemoApplication;
import br.edu.avenuecode.demo.models.Image;
import br.edu.avenuecode.demo.models.Product;
import br.edu.avenuecode.demo.services.ProductService;

/**
 * Set of TestCases for the Web Controller ImageController
 *
* @author <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * @created Jan 17, 2018
 * @since JDK 1.8
 */
@RunWith( SpringRunner.class )
@SpringBootTest(classes = DemoApplication.class, webEnvironment = WebEnvironment.DEFINED_PORT)
public class ImageControllerTests {
    
    private static final String PRODUCTS_ENDPOINT = "http://localhost:8080/products/";
    private static final String IMAGES_ENDPOINT = "http://localhost:8080/images/";
    
    private static final String ACCESSORIES = "Accessories";
    private static final String CLOTHES = "Clothes";
    private static final String SHOES = "Shoes";
    private static final String SHOES_DESC = "Description for shoes";
    private static final String HATS = "Hats";
    private static final String HATS_DESC = "Description for hats";
    
    @Autowired
    TestRestTemplate template;
    
    private Product shoes;
    
    private Product hats;
    
    private static long firstId = 0;
    
    private static long secondId = 0;
    
    @Autowired
    ProductService productService;
    
    @Before
    public void setUp() {
        
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-type", "application/json");
        
        Image accessories = new Image(ACCESSORIES);
        TestUtils.doPostRequest(template, IMAGES_ENDPOINT, headers, accessories, Image.class);
        firstId = TestUtils.nextImageId();
        
        Image clothes = new Image(CLOTHES);
        TestUtils.doPostRequest(template, IMAGES_ENDPOINT, headers, clothes, Image.class);
        secondId = TestUtils.nextImageId();
    }
    
    @After
    public void tearDown() {
        
      template.delete( IMAGES_ENDPOINT + "/" + secondId );
      template.delete( IMAGES_ENDPOINT + "/" + firstId );
      
      if ( shoes != null ) {
          
          productService.deleteProduct( shoes.getId() );
          shoes = null;
      }
      if ( hats != null ) {
          
          productService.deleteProduct( hats.getId() );
          hats = null;
      }
    }
    
    // ------------------------------------------------------------------------
    // READ operations
    // ------------------------------------------------------------------------
    
    /**
     * Test method for 
     * {@link br.edu.avenuecode.demo.controllers.rest.ImageController#getAllImages()}.
     * 
     * @since JDK 1.8
     */
    @Test
    public void testGetAllImages() {
        
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-type", "application/json");
        
        ResponseEntity<String> response = TestUtils.doGetRequest(template, IMAGES_ENDPOINT, headers, String.class);
        System.out.println( "[GET-ALL-IMAGES] Status: " + response.getStatusCodeValue() + " Headers: " + response.getHeaders() + " Body: " + response.getBody() );

        String expected = "[{\"id\":" + firstId + ",\"type\":\"Accessories\"},{\"id\":" + secondId + ",\"type\":\"Clothes\"}]";
        assertEquals( "Images are incorrect", expected, response.getBody() );
        
    }
    
    /**
     * Test method for 
     * {@link br.edu.avenuecode.demo.controllers.rest.ImageController#getImageById(Long)}.
     * 
     * @since JDK 1.8
     */
    @Test
    public void testGetImageById() throws JSONException {
        
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-type", "application/json");
        
        ResponseEntity<Image> response1 = TestUtils.doGetRequest(template, IMAGES_ENDPOINT + firstId, headers, Image.class);
        System.out.println( "[GET-BY-ID] Status: " + response1.getStatusCodeValue() + " Headers: " + response1.getHeaders() + " Body: " + response1.getBody() );
        assertEquals("Image is incorrect", ACCESSORIES, response1.getBody().getType() );
        
        ResponseEntity<Image> response2 = TestUtils.doGetRequest(template, IMAGES_ENDPOINT + secondId, headers, Image.class);
        System.out.println( "[GET-BY-ID] Status: " + response2.getStatusCodeValue() + " Headers: " + response2.getHeaders() + " Body: " + response2.getBody() );
        assertEquals("Image is incorrect", CLOTHES, response2.getBody().getType() );
    }
    
    /**
     * Test method for 
     * {@link br.edu.avenuecode.demo.controllers.rest.ImageController#getAllProductImages(Long)}.
     * 
     * @since JDK 1.8
     */
    @Test
    public void testGetAllProductImages() {
        
        HttpHeaders headers1 = new HttpHeaders();
        headers1.add("Content-type", "application/json");
        
        shoes = new Product(SHOES, SHOES_DESC);
        TestUtils.doPostRequest(template, PRODUCTS_ENDPOINT, headers1, shoes, Product.class);
        long pid1 = TestUtils.nextProductId();
        shoes.setId( pid1 );
        
        hats = new Product(HATS, HATS_DESC);
        TestUtils.doPostRequest(template, PRODUCTS_ENDPOINT, headers1, hats, Product.class);
        long pid2 = TestUtils.nextProductId();
        hats.setId( pid2 );
        
        Image insole = new Image("Insoles");
        TestUtils.doPostRequest(template, IMAGES_ENDPOINT, headers1, insole, Image.class);
        long id1 = TestUtils.nextImageId();
        
        Image carpet = new Image("Carpets");
        TestUtils.doPostRequest(template, IMAGES_ENDPOINT, headers1, carpet, Image.class);
        long id2 = TestUtils.nextImageId();
        
        HttpHeaders headers2 = new HttpHeaders();
        headers2.add("Content-type", "text/uri-list");
        
        TestUtils.doPutRequest(template, PRODUCTS_ENDPOINT + pid1 + "/images", headers2, IMAGES_ENDPOINT + id1, String.class);
        TestUtils.doPutRequest(template, PRODUCTS_ENDPOINT + pid2 + "/images", headers2, IMAGES_ENDPOINT + id2, String.class);
        
        ResponseEntity<String> response1 = TestUtils.doGetRequest(template, PRODUCTS_ENDPOINT + pid1 + "/images/", headers1, String.class);
        System.out.println( "[GET-ALL-SHOES-IMAGES] Status: " + response1.getStatusCodeValue() + " Headers: " + response1.getHeaders() + " Body: " + response1.getBody() );
        String expected1 = "[{\"id\":" + id1 + ",\"type\":\"Insoles\"}]";
        assertEquals( "Images are incorrect", expected1, response1.getBody() );
        
        ResponseEntity<String> response2 = TestUtils.doGetRequest(template, PRODUCTS_ENDPOINT + pid2 + "/images/", headers1, String.class);
        System.out.println( "[GET-ALL-HATS-IMAGES] Status: " + response2.getStatusCodeValue() + " Headers: " + response2.getHeaders() + " Body: " + response2.getBody() );
        String expected2 = "[{\"id\":" + id2 + ",\"type\":\"Carpets\"}]";
        assertEquals( "Images are incorrect", expected2, response2.getBody() );
    }
    
    /**
     * Test method for 
     * {@link br.edu.avenuecode.demo.controllers.rest.ImageController#getProductImageById(Long, Long)}.
     * 
     * @since JDK 1.8
     */
    @Test
    public void testGetProductImageById() {
        
        HttpHeaders headers1 = new HttpHeaders();
        headers1.add("Content-type", "application/json");
        
        shoes = new Product(SHOES, SHOES_DESC);
        TestUtils.doPostRequest(template, PRODUCTS_ENDPOINT, headers1, shoes, Product.class);
        long pid1 = TestUtils.nextProductId();
        shoes.setId( pid1 );
        
        Image insole = new Image("Insoles");
        TestUtils.doPostRequest(template, IMAGES_ENDPOINT, headers1, insole, Image.class);
        long id1 = TestUtils.nextImageId();
        
        Image carpet = new Image("Carpets");
        TestUtils.doPostRequest(template, IMAGES_ENDPOINT, headers1, carpet, Image.class);
        long id2 = TestUtils.nextImageId();
        
        HttpHeaders headers2 = new HttpHeaders();
        headers2.add("Content-type", "text/uri-list");
        
        TestUtils.doPutRequest(template, PRODUCTS_ENDPOINT + pid1 + "/images", headers2, IMAGES_ENDPOINT + id1, String.class);
        TestUtils.doPutRequest(template, PRODUCTS_ENDPOINT + pid1 + "/images", headers2, IMAGES_ENDPOINT + id2, String.class);
        
        ResponseEntity<Image> response1 = TestUtils.doGetRequest(template, PRODUCTS_ENDPOINT + pid1 + "/images/" + id1, headers1, Image.class);
        System.out.println( "[GET-INSOLES] Status: " + response1.getStatusCodeValue() + " Headers: " + response1.getHeaders() + " Body: " + response1.getBody() );
        assertEquals("Image is incorrect", "Insoles", response1.getBody().getType() );
        
        ResponseEntity<Image> response2 = TestUtils.doGetRequest(template, PRODUCTS_ENDPOINT + pid1 + "/images/" + id2, headers1, Image.class);
        System.out.println( "[GET-CARPETS] Status: " + response2.getStatusCodeValue() + " Headers: " + response2.getHeaders() + " Body: " + response2.getBody() );
        assertEquals("Image is incorrect", "Carpets", response2.getBody().getType() );
    }
    
    // ------------------------------------------------------------------------
    // CREATE operations
    // ------------------------------------------------------------------------
    
    /**
     * Test method for 
     * {@link br.edu.avenuecode.demo.controllers.rest.ImageController#createImage(Image)}.
     * 
     * @since JDK 1.8
     */
    @Test
    public void testCreateImage() {
        
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-type", "application/json");
        
        Image insole = new Image("Insoles");
        TestUtils.doPostRequest(template, IMAGES_ENDPOINT, headers, insole, Image.class);
        long id1 = TestUtils.nextImageId();
        
        Image carpet = new Image("Carpets");
        TestUtils.doPostRequest(template, IMAGES_ENDPOINT, headers, carpet, Image.class);
        long id2 = TestUtils.nextImageId();
        
        ResponseEntity<Image> response1 = TestUtils.doGetRequest(template, IMAGES_ENDPOINT + id1, headers, Image.class);
        System.out.println( "[GET-INSOLE] Status: " + response1.getStatusCodeValue() + " Headers: " + response1.getHeaders() + " Body: " + response1.getBody() );
        assertEquals("Image is incorrect", "Insoles", response1.getBody().getType() );
        
        ResponseEntity<Image> response2 = TestUtils.doGetRequest(template, IMAGES_ENDPOINT + id2, headers, Image.class);
        System.out.println( "[GET-CARPET] Status: " + response2.getStatusCodeValue() + " Headers: " + response2.getHeaders() + " Body: " + response2.getBody() );
        assertEquals("Image is incorrect", "Carpets", response2.getBody().getType() );
        
        template.delete( IMAGES_ENDPOINT + "/" + id1 );
        template.delete( IMAGES_ENDPOINT + "/" + id2 );
    }
    
    /**
     * Test method for 
     * {@link br.edu.avenuecode.demo.controllers.rest.ImageController#createProductImage(Image, Long)}.
     * 
     * @since JDK 1.8
     */
    @Test
    public void testCreateProductImage() {
        
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-type", "application/json");
        
        shoes = new Product(SHOES, SHOES_DESC);
        TestUtils.doPostRequest(template, PRODUCTS_ENDPOINT, headers, shoes, Product.class);
        long pid1 = TestUtils.nextProductId();
        shoes.setId( pid1 );
        
        hats = new Product(HATS, HATS_DESC);
        TestUtils.doPostRequest(template, PRODUCTS_ENDPOINT, headers, hats, Product.class);
        long pid2 = TestUtils.nextProductId();
        hats.setId( pid2 );
        
        Image insole = new Image("Insoles");
        TestUtils.doPostRequest(template, PRODUCTS_ENDPOINT + pid1 + "/images", headers, insole, Image.class);
        long id1 = TestUtils.nextImageId();
        
        Image carpet = new Image("Carpets");
        TestUtils.doPostRequest(template, PRODUCTS_ENDPOINT + pid2 + "/images", headers, carpet, Image.class);
        long id2 = TestUtils.nextImageId();
        
        ResponseEntity<Image> response1 = TestUtils.doGetRequest(template, PRODUCTS_ENDPOINT + pid1 + "/images/" + id1, headers, Image.class);
        System.out.println( "[GET-INSOLES] Status: " + response1.getStatusCodeValue() + " Headers: " + response1.getHeaders() + " Body: " + response1.getBody() );
        assertEquals("Image is incorrect", "Insoles", response1.getBody().getType() );
        
        ResponseEntity<Image> response2 = TestUtils.doGetRequest(template, PRODUCTS_ENDPOINT + pid2 + "/images/" + id2, headers, Image.class);
        System.out.println( "[GET-CARPETS] Status: " + response2.getStatusCodeValue() + " Headers: " + response2.getHeaders() + " Body: " + response2.getBody() );
        assertEquals("Image is incorrect", "Carpets", response2.getBody().getType() );
        
        System.out.println( "Done" );
    }
    
    // ------------------------------------------------------------------------
    // UPDATE operations
    // ------------------------------------------------------------------------
    
    /**
     * Test method for 
     * {@link br.edu.avenuecode.demo.controllers.rest.ImageController#updateImage(Image, Long)}.
     * 
     * @since JDK 1.8
     */
    @Test
    public void testUpdateImage() {
        
        Image updAccessories = new Image("Accessories-Women");
        Image updClothes = new Image("Clothes-Head");
        
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-type", "application/json");
        
        TestUtils.doPutRequest(template, IMAGES_ENDPOINT + firstId, headers, updAccessories, String.class);
        TestUtils.doPutRequest(template, IMAGES_ENDPOINT + secondId, headers, updClothes, String.class);
        
        ResponseEntity<Image> response1 = TestUtils.doGetRequest(template, IMAGES_ENDPOINT + firstId, headers, Image.class);
        System.out.println( "[GET-UPD-ACCESSORIES] Status: " + response1.getStatusCodeValue() + " Headers: " + response1.getHeaders() + " Body: " + response1.getBody() );
        assertEquals("Image is incorrect", "Accessories-Women", response1.getBody().getType() );
        
        ResponseEntity<Image> response2 = TestUtils.doGetRequest(template, IMAGES_ENDPOINT + secondId, headers, Image.class);
        System.out.println( "[GET-UPD-CLOTHES] Status: " + response2.getStatusCodeValue() + " Headers: " + response2.getHeaders() + " Body: " + response2.getBody() );
        assertEquals("Image is incorrect", "Clothes-Head", response2.getBody().getType() );
    }
    
    /**
     * Test method for 
     * {@link br.edu.avenuecode.demo.controllers.rest.ImageController#updateProjectImage(String, Long)}.
     * 
     * @since JDK 1.8
     */
    @Test
    public void testUpdateProjectImage() {
        
        HttpHeaders headers1 = new HttpHeaders();
        headers1.add("Content-type", "application/json");
        
        shoes = new Product(SHOES, SHOES_DESC);
        TestUtils.doPostRequest(template, PRODUCTS_ENDPOINT, headers1, shoes, Product.class);
        long pid1 = TestUtils.nextProductId();
        shoes.setId( pid1 );
        
        hats = new Product(HATS, HATS_DESC);
        TestUtils.doPostRequest(template, PRODUCTS_ENDPOINT, headers1, hats, Product.class);
        long pid2 = TestUtils.nextProductId();
        hats.setId( pid2 );
        
        HttpHeaders headers2 = new HttpHeaders();
        headers2.add("Content-type", "text/uri-list");

        TestUtils.doPutRequest(template, PRODUCTS_ENDPOINT + pid1 + "/images", headers2, IMAGES_ENDPOINT + firstId, String.class);
        TestUtils.doPutRequest(template, PRODUCTS_ENDPOINT + pid2 + "/images", headers2, IMAGES_ENDPOINT + secondId, String.class);
        
        ResponseEntity<Image> response1 = TestUtils.doGetRequest(template, PRODUCTS_ENDPOINT + pid1 + "/images/" + firstId, headers1, Image.class);
        System.out.println( "[GET-ACCESSORIES] Status: " + response1.getStatusCodeValue() + " Headers: " + response1.getHeaders() + " Body: " + response1.getBody() );
        assertEquals("Image is incorrect", "Accessories", response1.getBody().getType() );
        
        ResponseEntity<Image> response2 = TestUtils.doGetRequest(template, PRODUCTS_ENDPOINT + pid2 + "/images/" + secondId, headers1, Image.class);
        System.out.println( "[GET-CLOTHES] Status: " + response2.getStatusCodeValue() + " Headers: " + response2.getHeaders() + " Body: " + response2.getBody() );
        assertEquals("Image is incorrect", "Clothes", response2.getBody().getType() );
    }
    
    // ------------------------------------------------------------------------
    // DELETE operations
    // ------------------------------------------------------------------------
    
    /**
     * Test method for 
     * {@link br.edu.avenuecode.demo.controllers.rest.ImageController#deleteImage(Long)}.
     * 
     * @since JDK 1.8
     */
    @Test
    public void testDeleteImage() {
        
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-type", "application/json");
        
        Image insole = new Image("Insoles");
        TestUtils.doPostRequest(template, IMAGES_ENDPOINT, headers, insole, Image.class);
        long id1 = TestUtils.nextImageId();
        
        Image carpet = new Image("Carpets");
        TestUtils.doPostRequest(template, IMAGES_ENDPOINT, headers, carpet, Image.class);
        long id2 = TestUtils.nextImageId();
        
        ResponseEntity<Image> response1 = TestUtils.doGetRequest(template, IMAGES_ENDPOINT + id1, headers, Image.class);
        System.out.println( "[GET-INSOLE] Status: " + response1.getStatusCodeValue() + " Headers: " + response1.getHeaders() + " Body: " + response1.getBody() );
        assertEquals("Image is incorrect", "Insoles", response1.getBody().getType() );
        
        ResponseEntity<Image> response2 = TestUtils.doGetRequest(template, IMAGES_ENDPOINT + id2, headers, Image.class);
        System.out.println( "[GET-CARPET] Status: " + response2.getStatusCodeValue() + " Headers: " + response2.getHeaders() + " Body: " + response2.getBody() );
        assertEquals("Image is incorrect", "Carpets", response2.getBody().getType() );
        
        template.delete( IMAGES_ENDPOINT + "/" + id1 );
        template.delete( IMAGES_ENDPOINT + "/" + id2 );
        
        ResponseEntity<Image> response3 = TestUtils.doGetRequest(template, IMAGES_ENDPOINT + id1, headers, Image.class);
        System.out.println( "[GET-DEL-INSOLE] Status: " + response3.getStatusCodeValue() + " Headers: " + response3.getHeaders() + " Body: " + response3.getBody() );
        assertNull("Image is still there", response3.getBody() );
        
        ResponseEntity<Image> response4 = TestUtils.doGetRequest(template, IMAGES_ENDPOINT + id2, headers, Image.class);
        System.out.println( "[GET-CARPET] Status: " + response4.getStatusCodeValue() + " Headers: " + response4.getHeaders() + " Body: " + response4.getBody() );
        assertNull("Image is still there", response4.getBody() );
    }
}
