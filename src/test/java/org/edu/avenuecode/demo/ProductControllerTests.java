/*****************************************************************************
 * Project:   <B>AvenueTestDemo</B>
 * File:      <I>ProductControllerTests.java</I>
 * Author:    <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * Creation:  Jan 17, 2018
 * --------------------------------------------------------------------------
 * <I>Copyright 2018, <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * All rights are reserved. Reproduction in whole or part is prohibited
 * without the written consent of the copyright owner.</I>
 *****************************************************************************/
package org.edu.avenuecode.demo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import br.edu.avenuecode.demo.DemoApplication;
import br.edu.avenuecode.demo.models.Product;

/**
 * Set of TestCases for the Web Controller ProductController
 *
* @author <B>H</B>ugo <B>S</B>aporetti <B>J</B>unior
 * @created Jan 17, 2018
 * @since JDK 1.8
 */
@RunWith( SpringRunner.class )
@SpringBootTest(classes = DemoApplication.class, webEnvironment = WebEnvironment.DEFINED_PORT)
public class ProductControllerTests {
    
    private static final String PRODUCTS_ENDPOINT = "http://localhost:8080/products/";
    private static final String PRODUCTS_DTO_ENDPOINT = "http://localhost:8080/products/dto/";
    
    private static final String SHOES = "Shoes";
    private static final String SHOES_DESC = "Description for shoes";
    private static final String HATS = "Hats";
    private static final String HATS_DESC = "Description for hats";
    
    @Autowired
    TestRestTemplate template;
    
    private static long firstId = 0;
    
    private static long secondId = 0;
    
    @Before
    public void setUp() {
        
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-type", "application/json");
        
        Product shoes = new Product(SHOES, SHOES_DESC);
        TestUtils.doPostRequest(template, PRODUCTS_ENDPOINT, headers, shoes, Product.class);
        firstId = TestUtils.nextProductId();
        
        Product hats = new Product(HATS, HATS_DESC);
        TestUtils.doPostRequest(template, PRODUCTS_ENDPOINT, headers, hats, Product.class);
        secondId = TestUtils.nextProductId();
    }
    
    @After
    public void tearDown() {
        
      template.delete( PRODUCTS_ENDPOINT + "/" + secondId );
      template.delete( PRODUCTS_ENDPOINT + "/" + firstId );
    }
    
    // ------------------------------------------------------------------------
    // READ operations
    // ------------------------------------------------------------------------
    
    /**
     * Test method for 
     * {@link br.edu.avenuecode.demo.controllers.rest.ProductController#getAllProducts()}.
     * 
     * @since JDK 1.8
     */
    @Test
    public void testGetAllProducts() {
        
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-type", "application/json");
        
        ResponseEntity<String> response = TestUtils.doGetRequest(template, PRODUCTS_ENDPOINT, headers, String.class);
        System.out.println( "[GET-ALL-PRODUCTS] Status: " + response.getStatusCodeValue() + " Headers: " + response.getHeaders() + " Body: " + response.getBody() );

        String expected = "[{\"id\":" + firstId + ",\"name\":\"Shoes\",\"description\":\"Description for shoes\",\"images\":[],\"products\":[]},{\"id\":" + secondId + ",\"name\":\"Hats\",\"description\":\"Description for hats\",\"images\":[],\"products\":[]}]";
        assertEquals( "Products are incorrect", expected, response.getBody() );
        
        System.out.println( "Done" );
    }
    
    /**
     * Test method for 
     * {@link br.edu.avenuecode.demo.controllers.rest.ProductController#getProductById(Long)}.
     * 
     * @since JDK 1.8
     */
    @Test
    public void testGetProductById() {
        
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-type", "application/json");
        
        ResponseEntity<Product> response1 = TestUtils.doGetRequest(template, PRODUCTS_ENDPOINT + firstId, headers, Product.class);
        System.out.println( "[GET-BY-ID] Status: " + response1.getStatusCodeValue() + " Headers: " + response1.getHeaders() + " Body: " + response1.getBody() );
        assertEquals("Product is incorrect", SHOES, response1.getBody().getName() );
        
        ResponseEntity<Product> response2 = TestUtils.doGetRequest(template, PRODUCTS_ENDPOINT + secondId, headers, Product.class);
        System.out.println( "[GET-BY-ID] Status: " + response2.getStatusCodeValue() + " Headers: " + response2.getHeaders() + " Body: " + response2.getBody() );
        assertEquals("Product is incorrect", HATS, response2.getBody().getName() );
    }
    
    /**
     * Test method for 
     * {@link br.edu.avenuecode.demo.controllers.rest.ProductController#getParentProducts(Long)}.
     * 
     * @since JDK 1.8
     */
    @Test
    public void testGetParentProducts() {
        
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-type", "application/json");
        
        Product ball = new Product("Ball", "Ball description");
        TestUtils.doPostRequest(template, PRODUCTS_ENDPOINT + firstId + "/products", headers, ball, Product.class);
        long pid1 = TestUtils.nextProductId();
        
        Product toy = new Product("Toy", "Toy description");
        TestUtils.doPostRequest(template, PRODUCTS_ENDPOINT + secondId + "/products", headers, toy, Product.class);
        long pid2 = TestUtils.nextProductId();
        
        ResponseEntity<Product> response1 = TestUtils.doGetRequest(template, PRODUCTS_ENDPOINT + firstId + "/products/" + pid1, headers, Product.class);
        System.out.println( "[GET-BALL] Status: " + response1.getStatusCodeValue() + " Headers: " + response1.getHeaders() + " Body: " + response1.getBody() );
        assertEquals("Product is incorrect", "Ball", response1.getBody().getName() );
        
        ResponseEntity<Product> response2 = TestUtils.doGetRequest(template, PRODUCTS_ENDPOINT + secondId + "/products/" + pid2, headers, Product.class);
        System.out.println( "[GET-TOY] Status: " + response2.getStatusCodeValue() + " Headers: " + response2.getHeaders() + " Body: " + response2.getBody() );
        assertEquals("Product is incorrect", "Toy", response2.getBody().getName() );
        
        template.delete( PRODUCTS_ENDPOINT + firstId + "/products" );
        template.delete( PRODUCTS_ENDPOINT + secondId + "/products" );
        
        System.out.println( "Done" );
    }
    
    // DTO Tests
    
    /**
     * Test method for 
     * {@link br.edu.avenuecode.demo.controllers.rest.ProductController#getAllDtoProducts()}.
     * 
     * @since JDK 1.8
     */
    @Test
    public void testGetAllDtoProducts() {
        
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-type", "application/json");
        
        ResponseEntity<String> response = TestUtils.doGetRequest(template, PRODUCTS_DTO_ENDPOINT, headers, String.class);
        System.out.println( "[GET-ALL-DTO-PRODUCTS] Status: " + response.getStatusCodeValue() + " Headers: " + response.getHeaders() + " Body: " + response.getBody() );

        String expected = "[{\"id\":" + firstId + ",\"name\":\"Shoes\",\"description\":\"Description for shoes\"},{\"id\":" + secondId + ",\"name\":\"Hats\",\"description\":\"Description for hats\"}]";
        assertEquals( "Products are incorrect", expected, response.getBody() );
    }
    
    /**
     * Test method for 
     * {@link br.edu.avenuecode.demo.controllers.rest.ProductController#getDtoProductById(Long)}.
     * 
     * @since JDK 1.8
     */
    @Test
    public void testGetDtoProductById() {
        
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-type", "application/json");
        
        ResponseEntity<String> response1 = TestUtils.doGetRequest(template, PRODUCTS_DTO_ENDPOINT + firstId, headers, String.class);
        System.out.println( "[GET-DTO-BY-ID] Status: " + response1.getStatusCodeValue() + " Headers: " + response1.getHeaders() + " Body: " + response1.getBody() );
        String expected1 = "{\"id\":" + firstId + ",\"name\":\"Shoes\",\"description\":\"Description for shoes\"}";
        assertEquals( "Product is incorrect", expected1, response1.getBody() );
        
        ResponseEntity<String> response2 = TestUtils.doGetRequest(template, PRODUCTS_DTO_ENDPOINT + secondId, headers, String.class);
        System.out.println( "[GET-DTO-BY-ID] Status: " + response2.getStatusCodeValue() + " Headers: " + response2.getHeaders() + " Body: " + response2.getBody() );
        String expected2 = "{\"id\":" + secondId + ",\"name\":\"Hats\",\"description\":\"Description for hats\"}";
        assertEquals( "Product is incorrect", expected2, response2.getBody() );
    }
    
    /**
     * Test method for 
     * {@link br.edu.avenuecode.demo.controllers.rest.ProductController#getDtoParentProducts(Long)}.
     * 
     * @since JDK 1.8
     */
    @Test
    public void testGetDtoParentProducts() {
        
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-type", "application/json");
        
        Product ball = new Product("Ball", "Ball description");
        TestUtils.doPostRequest(template, PRODUCTS_ENDPOINT + firstId + "/products", headers, ball, Product.class);
        long pid1 = TestUtils.nextProductId();
        
        Product toy = new Product("Toy", "Toy description");
        TestUtils.doPostRequest(template, PRODUCTS_ENDPOINT + secondId + "/products", headers, toy, Product.class);
        long pid2 = TestUtils.nextProductId();
        
        ResponseEntity<String> response1 = TestUtils.doGetRequest(template, PRODUCTS_DTO_ENDPOINT + firstId + "/products", headers, String.class);
        System.out.println( "[GET-DTO-BALL] Status: " + response1.getStatusCodeValue() + " Headers: " + response1.getHeaders() + " Body: " + response1.getBody() );
        String expected1 = "[{\"id\":" + pid1 + ",\"name\":\"Ball\",\"description\":\"Ball description\"}]";
        assertEquals( "Product is incorrect", expected1, response1.getBody() );
        
        ResponseEntity<String> response2 = TestUtils.doGetRequest(template, PRODUCTS_DTO_ENDPOINT + secondId + "/products", headers, String.class);
        System.out.println( "[GET-DTO-TOY] Status: " + response2.getStatusCodeValue() + " Headers: " + response2.getHeaders() + " Body: " + response2.getBody() );
        String expected2 = "[{\"id\":" + pid2 + ",\"name\":\"Toy\",\"description\":\"Toy description\"}]";
        assertEquals( "Product is incorrect", expected2, response2.getBody() );

    }
    
    // ------------------------------------------------------------------------
    // CREATE operations
    // ------------------------------------------------------------------------
    
    /**
     * Test method for 
     * {@link br.edu.avenuecode.demo.controllers.rest.ProductController#createProduct(Product)}.
     * 
     * @since JDK 1.8
     */
    @Test
    public void testCreateProduct() {
        
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-type", "application/json");
        
        Product ball = new Product("Ball", "Ball description");
        TestUtils.doPostRequest(template, PRODUCTS_ENDPOINT, headers, ball, Product.class);
        long id1 = TestUtils.nextProductId();
        
        Product toy = new Product("Toy", "Toy description");
        TestUtils.doPostRequest(template, PRODUCTS_ENDPOINT, headers, toy, Product.class);
        long id2 = TestUtils.nextProductId();
        
        ResponseEntity<Product> response1 = TestUtils.doGetRequest(template, PRODUCTS_ENDPOINT + id1, headers, Product.class);
        System.out.println( "[GET-BALL] Status: " + response1.getStatusCodeValue() + " Headers: " + response1.getHeaders() + " Body: " + response1.getBody() );
        assertEquals("Product is incorrect", "Ball", response1.getBody().getName() );
        
        ResponseEntity<Product> response2 = TestUtils.doGetRequest(template, PRODUCTS_ENDPOINT + id2, headers, Product.class);
        System.out.println( "[GET-TOY] Status: " + response2.getStatusCodeValue() + " Headers: " + response2.getHeaders() + " Body: " + response2.getBody() );
        assertEquals("Product is incorrect", "Toy", response2.getBody().getName() );
        
        template.delete( PRODUCTS_ENDPOINT + "/" + id1 );
        template.delete( PRODUCTS_ENDPOINT + "/" + id2 );
    }
    
    /**
     * Test method for 
     * {@link br.edu.avenuecode.demo.controllers.rest.ProductController#createParentProduct(Product, Long)}.
     * 
     * @since JDK 1.8
     */
    @Test
    public void testCreateParentProduct() {
        
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-type", "application/json");
        
        Product ball = new Product("Ball", "Ball description");
        TestUtils.doPostRequest(template, PRODUCTS_ENDPOINT + firstId + "/products", headers, ball, Product.class);
        long pid1 = TestUtils.nextProductId();
        
        Product toy = new Product("Toy", "Toy description");
        TestUtils.doPostRequest(template, PRODUCTS_ENDPOINT + secondId + "/products", headers, toy, Product.class);
        long pid2 = TestUtils.nextProductId();
        
        ResponseEntity<String> response = TestUtils.doGetRequest(template, PRODUCTS_ENDPOINT, headers, String.class);
        System.out.println( "[GET-ALL-PRODUCT-PRODUCTS] Status: " + response.getStatusCodeValue() + " Headers: " + response.getHeaders() + " Body: " + response.getBody() );

        String expected = "[{\"id\":" + firstId + ",\"name\":\"Shoes\",\"description\":\"Description for shoes\",\"images\":[],\"products\":[{\"id\":" + pid1 + ",\"name\":\"Ball\",\"description\":\"Ball description\",\"images\":[],\"products\":[]}]},{\"id\":" + secondId + ",\"name\":\"Hats\",\"description\":\"Description for hats\",\"images\":[],\"products\":[{\"id\":" + pid2 + ",\"name\":\"Toy\",\"description\":\"Toy description\",\"images\":[],\"products\":[]}]},{\"id\":" + pid1 + ",\"name\":\"Ball\",\"description\":\"Ball description\",\"images\":[],\"products\":[]},{\"id\":" + pid2 + ",\"name\":\"Toy\",\"description\":\"Toy description\",\"images\":[],\"products\":[]}]";
        assertEquals( "Products are incorrect", expected, response.getBody() );
    }
    
    // ------------------------------------------------------------------------
    // UPDATE operations
    // ------------------------------------------------------------------------
    
    /**
     * Test method for 
     * {@link br.edu.avenuecode.demo.controllers.rest.ProductController#updateProduct(Product, Long)}.
     * 
     * @since JDK 1.8
     */
    @Test
    public void testUpdateProduct() {
        
        Product updShoes = new Product("RED Shoes", "Description for red shoes");
        Product updHats = new Product("RED Hats", "Description for red hats");
        
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-type", "application/json");
        
        TestUtils.doPutRequest(template, PRODUCTS_ENDPOINT + firstId, headers, updShoes, Product.class);
        TestUtils.doPutRequest(template, PRODUCTS_ENDPOINT + secondId, headers, updHats, Product.class);
        
        ResponseEntity<Product> response1 = TestUtils.doGetRequest(template, PRODUCTS_ENDPOINT + firstId, headers, Product.class);
        System.out.println( "[GET-UPD-SHOES] Status: " + response1.getStatusCodeValue() + " Headers: " + response1.getHeaders() + " Body: " + response1.getBody() );
        assertEquals("Product is incorrect", "RED Shoes", response1.getBody().getName() );
        
        ResponseEntity<Product> response2 = TestUtils.doGetRequest(template, PRODUCTS_ENDPOINT + secondId, headers, Product.class);
        System.out.println( "[GET-UPD-HATS] Status: " + response2.getStatusCodeValue() + " Headers: " + response2.getHeaders() + " Body: " + response2.getBody() );
        assertEquals("Product is incorrect", "RED Hats", response2.getBody().getName() );
    }
    
    /**
     * Test method for 
     * {@link br.edu.avenuecode.demo.controllers.rest.ProductController#updateParentProduct(String, Long)}.
     * 
     * @since JDK 1.8
     */
    @Test
    public void testUpdateParentProduct() {
        
        HttpHeaders headers1 = new HttpHeaders();
        headers1.add("Content-type", "application/json");
        
        Product ball = new Product("Ball", "Ball description");
        TestUtils.doPostRequest(template, PRODUCTS_ENDPOINT, headers1, ball, Product.class);
        long pid1 = TestUtils.nextProductId();
        
        Product toy = new Product("Toy", "Toy description");
        TestUtils.doPostRequest(template, PRODUCTS_ENDPOINT, headers1, toy, Product.class);
        long pid2 = TestUtils.nextProductId();
        
        HttpHeaders headers2 = new HttpHeaders();
        headers2.add("Content-type", "text/uri-list");
        
        TestUtils.doPutRequest(template, PRODUCTS_ENDPOINT + firstId + "/products", headers2, PRODUCTS_ENDPOINT + pid1, String.class);
        TestUtils.doPutRequest(template, PRODUCTS_ENDPOINT + secondId + "/products", headers2, PRODUCTS_ENDPOINT + pid2, String.class);
        
        ResponseEntity<String> response = TestUtils.doGetRequest(template, PRODUCTS_ENDPOINT, headers1, String.class);
        System.out.println( "[GET-ALL-PRODUCT-PRODUCTS] Status: " + response.getStatusCodeValue() + " Headers: " + response.getHeaders() + " Body: " + response.getBody() );
        
        String expected = "[{\"id\":" + firstId + ",\"name\":\"Shoes\",\"description\":\"Description for shoes\",\"images\":[],\"products\":[{\"id\":" + pid1 + ",\"name\":\"Ball\",\"description\":\"Ball description\",\"images\":[],\"products\":[]}]},{\"id\":" + secondId + ",\"name\":\"Hats\",\"description\":\"Description for hats\",\"images\":[],\"products\":[{\"id\":" + pid2 + ",\"name\":\"Toy\",\"description\":\"Toy description\",\"images\":[],\"products\":[]}]},{\"id\":" + pid1 + ",\"name\":\"Ball\",\"description\":\"Ball description\",\"images\":[],\"products\":[]},{\"id\":" + pid2 + ",\"name\":\"Toy\",\"description\":\"Toy description\",\"images\":[],\"products\":[]}]";
        assertEquals( "Products are incorrect", expected, response.getBody() );
        
        template.delete( PRODUCTS_ENDPOINT + pid1 );
        template.delete( PRODUCTS_ENDPOINT + pid2 );
        
        System.out.println( "Done" );
    }
    
    // ------------------------------------------------------------------------
    // DELETE operations
    // ------------------------------------------------------------------------
    
    /**
     * Test method for 
     * {@link br.edu.avenuecode.demo.controllers.rest.ProductController#deleteProduct(Long)}.
     * 
     * @since JDK 1.8
     */
    @Test
    public void testDeleteProduct() {
        
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-type", "application/json");
        
        Product ball = new Product("Ball", "Ball description");
        TestUtils.doPostRequest(template, PRODUCTS_ENDPOINT, headers, ball, Product.class);
        long id1 = TestUtils.nextProductId();
        
        Product toy = new Product("Toy", "Toy description");
        TestUtils.doPostRequest(template, PRODUCTS_ENDPOINT, headers, toy, Product.class);
        long id2 = TestUtils.nextProductId();
        
        ResponseEntity<Product> response1 = TestUtils.doGetRequest(template, PRODUCTS_ENDPOINT + id1, headers, Product.class);
        System.out.println( "[GET-BALL] Status: " + response1.getStatusCodeValue() + " Headers: " + response1.getHeaders() + " Body: " + response1.getBody() );
        assertEquals("Product is incorrect", "Ball", response1.getBody().getName() );
        
        ResponseEntity<Product> response2 = TestUtils.doGetRequest(template, PRODUCTS_ENDPOINT + id2, headers, Product.class);
        System.out.println( "[GET-TOY] Status: " + response2.getStatusCodeValue() + " Headers: " + response2.getHeaders() + " Body: " + response2.getBody() );
        assertEquals("Product is incorrect", "Toy", response2.getBody().getName() );
        
        template.delete( PRODUCTS_ENDPOINT + "/" + id1 );
        template.delete( PRODUCTS_ENDPOINT + "/" + id2 );
        
        ResponseEntity<Product> response3 = TestUtils.doGetRequest(template, PRODUCTS_ENDPOINT + id1, headers, Product.class);
        System.out.println( "[GET-DEL-BALL] Status: " + response3.getStatusCodeValue() + " Headers: " + response3.getHeaders() + " Body: " + response3.getBody() );
        assertNull("Product is still there", response3.getBody() );
        
        ResponseEntity<Product> response4 = TestUtils.doGetRequest(template, PRODUCTS_ENDPOINT + id2, headers, Product.class);
        System.out.println( "[GET-DEL-TOY] Status: " + response4.getStatusCodeValue() + " Headers: " + response4.getHeaders() + " Body: " + response4.getBody() );
        assertNull("Product is still there", response4.getBody() );
    }
}
