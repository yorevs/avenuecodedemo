# AvenueCode - Demo TODO list
-----

## GUI

* Raw Rest Call with body and address fields

## Documentation

* Finish Javadoc  
	* Add README.md topics:  
	* Add "2 Additional information"  

## Good to Have

* A persistent database being used  
* Logs and internationalisation  
* Various comments on the code  

