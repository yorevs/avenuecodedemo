# Avenue Code Challenge
------

This project contains a Demo App. required by the recruitment process for AvenueCode.


## 1 Technical specification


### 1.1 Build, install and run


#### 1.1.1 How to compile the application

The application uses Maven, so it is easy to compile the application.

- Use the Maven script provided: ```#> mvnw clean install```


#### 1.1.2 How to run the application

The application uses Spring Boot, so it is easy to run. You can start it any of a few ways:

- Run the main method from DemoApplicationTests.java
- Use the Maven script and the Spring Boot plugin: ```#> mvnw spring-boot:run```
- Package the application as a JAR and run it using java -jar demo-0.0.1-SNAPSHOT.jar

To view the running application visit the page: <http://localhost:8080/>

- To check the products API go to <http://localhost:8080/products>
- To check the images API go to <http://localhost:8080/images>


#### 1.1.3 How to run the test suite

The application uses Maven, so it is easy to run the test suite.

- Use the Maven script provided: ```#> mvnw test```


### 1.2 Building the Database

The application use an embedded in-memory database, and uses JPA annotations to configure and structure the database, which is automatically built at the startup process.

### 1.3 API specification


#### 1.3.1 Products


##### 1.3.1.1 Read operations


###### A. Get all products regardless of any parent product
- URL: /products
- Method: GET
- Headers: [Content-Type] = application/json
- Body: empty
- Return: JSON text representing an array of all system products including specified relationships (child product and/or images)


###### B. Get all products regardless of any parent product (EXCLUDING RELATIONS)
- URL: /products/dto
- Method: GET
- Headers: [Content-Type] = application/json
- Body: empty
- Return: JSON text representing an array of all system products excluding relationships (child products, images)


###### C. Get one product based on a specified ID
- URL: /products/{productId}
- Method: GET
- Headers: [Content-Type] = application/json
- Body: empty
- Return: JSON text representing the specified product including specified relationships (child product and/or images)


###### D. Get one product based on a specified ID (EXCLUDING RELATIONS)
- URL: /products/dto/{productId}
- Method: GET
- Headers: [Content-Type] = application/json
- Body: empty
- Return: JSON text representing the specified product excluding relationships (child products, images)


###### E. Get all products of a specified parent product
- URL: /products/{productId}/products
- Method: GET
- Headers: [Content-Type] = application/json
- Body: empty
- Return: JSON text representing an array of all product related products including specified relationships (child product and/or images)


###### F. Get all products of a specified parent product (EXCLUDING RELATIONS)
- URL: /products/dto/{productId}/products
- Method: GET
- Headers: [Content-Type] = application/json
- Body: empty
- Return: JSON text representing an array of all product related products excluding relationships (child products, images)


##### 1.3.1.2 Create operations

###### A. Create a new product regardless of any parent product
- URL: /products
- Method: POST
- Headers: [Content-Type] = application/json
- Body: 
    ```
    {
        "name": "<product_name>",
        "description": "<product_description>",
        "images": [ <image1_json>, <image2_json>, ... ],
        "products": [ <child1_prod_json>, <child2_prod_json>, ... ],
        "parent": <parent_project>
    }
    ```
- Return: empty


###### B. Create a new product for a specific parent product
- URL: /products/{productId}/products
- Method: POST
- Headers: [Content-Type] = application/json
- Body: 
    ```
    {
        "name": "<product_name>",
        "description": "<product_description>",
        "images": [ <image1_json>, <image2_json>, ... ],
        "products": [ <child1_prod_json>, <child2_prod_json>, ... ]
    }
    ```
- Return: empty


##### 1.3.1.3 Update operations

###### A. Update an existing product
- URL: /products/{productId}
- Method: PUT
- Headers: [Content-Type] = application/json
- Body: 
    ```
    {
        "name": "<upd_product_name>",
        "description": "<upd_product_description>",
        "images": [ <upd_image1_json>, <upd_image2_json>, ... ],
        "products": [ <upd_child1_prod_json>, <upd_child2_prod_json>, ... ]
    }
    ```
- Return: empty


###### B. Associate an existing product to a parent product
- URL: /products/{productId}/products
- Method: PUT
- Headers: [Content-Type] = text/uri-list
- Body: 
    ```
        <product_url>
        <product_url>
    ```
- Return: empty


##### 1.3.1.4 Delete operations

###### A. Delete an existing product
- URL: /products/{productId}
- Method: DELETE
- Headers: empty
- Body: empty
- Return: empty


#### 1.3.2 Images


##### 1.3.2.1 Read operations

###### A. Get all images regardless of any parent product
- URL: /images
- Method: GET
- Headers: [Content-Type] = application/json
- Body: empty
- Return: JSON text representing an array of all system images


###### B. Get one image based on a specified ID
- URL: /images/{imageId}
- Method: GET
- Headers: [Content-Type] = application/json
- Body: empty
- Return: JSON text representing the specified image


###### C. Get all images related to a specified parent product
- URL: /products/{productId}/images
- Method: GET
- Headers: [Content-Type] = application/json
- Body: empty
- Return: JSON text representing an array of all product related images


###### D. Get a specific image of a specified parent product
- URL: /products/{productId}/images/{imageId}
- Method: GET
- Headers: [Content-Type] = application/json
- Body: empty
- Return: JSON text representing the specified image of the specified parent product


##### 1.3.2.2 Create operations

###### A. Create a new image regardless of any parent product
- URL: /images
- Method: POST
- Headers: [Content-Type] = application/json
- Body: 
    ```
    {
        "type": "<image_name>",
        "product": <parent_project>
    }
    ```
- Return: empty


###### B. Create a new image for a specified parent product
- URL: /products/{productId}/images
- Method: POST
- Headers: [Content-Type] = application/json
- Body: 
    ```
    {
        "type": "<image_name>"
    }
    ```
- Return: empty


##### 1.3.2.3 Update operations

###### A. Update an existing image
- URL: /image/{imageId}
- Method: PUT
- Headers: [Content-Type] = application/json
- Body: 
    ```
    {
        "type": "<upd_ image_type>"
    }
    ```
- Return: empty

###### B. Associate an existing image to a product
- URL: /products/{productId}/images
- Method: PUT
- Headers: [Content-Type] = text/uri-list
- Body: 
    ```
        <image1_url>
        <image2_url>
    ```
- Return: empty


##### 1.3.2.4 Delete operations

###### A. Delete an existing image
- URL: /images/{imageId}
- Method: DELETE
- Headers: empty
- Body: empty
- Return: empty
